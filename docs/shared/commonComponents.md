* **Squash Orchestrator**†  
**Squash Orchestrator** est composé d’un ensemble de micro-services exploitables via l’envoi d’un plan d’exécution sous un formalisme bien précis, le PEaC (Plan d’Exécution «as Code»), afin d’orchestrer des exécutions de tests automatisés.  
Il dirige et coordonne les différents composants de la chaîne d'exécution de vos tests automatisés (environnements d'exécution, automates, reporting...).  
Il est basé sur l'**OpenTestFactory Orchestrator** tout en ajoutant un ensemble de micro-services pour étendre ses possibilités, notamment le fait d'exploiter des plans d'exécution **Squash TM** ou d'effectuer du reporting vers ce dernier.

* **Agent OpenTestFactory**†  
Cet agent permet des communications via le protocole HTTP entre un **Squash Orchestrator** et un environnement d'exécution de tests.  
Il s'agit d'un process qui tourne sur l'environnement d'exécution. Ce process contacte le **Squash Orchestrator** à intervalle régulier, à la recherche d'ordres à exécuter. S'il y a un ordre en attente, l'agent va l'exécuter puis retourner le résultat à l'orchestrateur.  
Il est actuellement nécessaire pour l'exécution de tests Agilitest, Ranorex ou UFT.
