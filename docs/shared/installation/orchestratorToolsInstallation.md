## Outils de l'orchestrateur†

### Installation

Les outils de l'orchestrateur sont destinés à simplifier l'exploitation de ce dernier.  
Ils requièrent Python 3.8 ou supérieur. Ils fonctionnent sur un environnement Linux, macOS ou Windows.

Afin d'installer ces outils, la commande suivante est à exécuter :

```bash
pip3 install --upgrade opentf-tools
```

### Aperçu des capacités des outils

* <a href="https://opentestfactory.gitlab.io/orchestrator/tools/start-and-wait/" target="_blank">`opentf-ready`</a> attend que l'orchestrateur soit prêt à accepter les workflows.

* <a href="https://opentestfactory.gitlab.io/orchestrator/tools/wait-and-stop/" target="_blank">`opentf-done`</a> attend que l'orchestrateur puisse être arrêté en toute sécurité (c'est-à-dire qu'il n'a plus de tâches en attente).

* <a href="https://opentestfactory.gitlab.io/orchestrator/tools/running-commands/" target="_blank">`openf-ctl`</a> peut être utilisé pour

    * démarrer un workflow
    * arrêter un workflow
    * annuler un workflow
    * générer un jeton signé
    * lister les abonnements sur le bus d'événements
