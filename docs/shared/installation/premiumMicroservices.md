## Micro-services exclusifs à Squash AUTOM Premium

### Installation

Afin d'installer l'image Docker des services exclusifs à **Squash AUTOM Premium**, vous devez récupérer l'image compressée auprès du service support Squash puis exécuter la commande suivante :

```bash
docker load -i squash-autom-premium-3.0.2.tar.gz
```

Les préconisations pour dimensionner le conteneur Docker sont :

* 1 CPU
* RAM : 256 Mo (1 Go recommandé)
* disque : prévoir la capacité de stocker les rapports d’exécution et leurs attachements pour une heure d'utilisation

### Usage

Pour exécuter cette image Docker, la commande suivante est à exécuter :

```bash
docker run -e BUS_HOST=<IP ou nom DNS du bus> \
           -e BUS_PORT=<port> -e BUS_TOKEN=<token JWT> \
           -e EXTERNAL_HOSTNAME=<IP exposée des micro-services> \
           --volume /chemin/vers/clef/publiques(s):/etc/squashtf/ \
           --volume /chemin/vers/dossier/temporaire:/tmp/ \
           squash-autom-premium:3.0.2
```

avec :

* *BUS_HOST* (obligatoire) : l'adresse IP ou le nom DNS du service *EventBus* du **Squash Orchestrator** avec qui les micro-services communiquent.
* *BUS_PORT* (obligatoire) : port du service *EventBus* du **Squash Orchestrator** avec qui les micro-services communiquent.
* *BUS_TOKEN* (facultatif) : token JWT accepté par le service *EventBus* du **Squash Orchestrator** avec qui les micro-services communiquent.
* *EXTERNAL_HOSTNAME* (facultatif) : l'adresse IP ou le nom DNS des micro-services de l'image Docker des services exclusifs.
* *--volume /etc/squashtf/* : volume à monter vers un ensemble de clés publiques acceptées pour la validité de tokens JWT.  
  Il est à mettre en place si une validation de token JWT est nécessaire pour l'échange de messages entre micro-services.
* *--volume /tmp/* : volume à monter vers un dossier partagé avec **Squash Orchestrator** qui accueillera temporairement des fichiers générés lors de l'exécution