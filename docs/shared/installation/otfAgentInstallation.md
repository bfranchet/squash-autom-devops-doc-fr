## Agent OpenTestFactory†

### Installation

L'agent OpenTestFactory est une application Python à installer sur un environnement d'exécution de tests automatisés.
Il requiert Python 3.8 ou supérieur. Il fonctionne sur un environnement Linux, macOS ou Windows.

L'agent se présente comme un simple script. Il possède seulement une dépendance, vers la librairie python ``requests`` (elle sera installée si elle n'est pas déjà présente sur l'environnement d'exécution).

Afin d'installer l'agent, la commande suivante est à exécuter :

```bash
pip3 install --upgrade opentf-agent
```

Vous pouvez vérifier l'installation en exécutant la commande suivante :

```bash
opentf-agent --help
```

### Usage

#### Résumé

```bash
$ opentf-agent --help
usage: opentf-agent [-h] --tags TAGS --host HOST [--port PORT] [--path_prefix PATH_PREFIX] [--token TOKEN] [--encoding ENCODING] [--script_path SCRIPT_PATH] [--workspace_dir WORKSPACE_DIR] [--name NAME] [--polling_delay POLLING_DELAY] [--liveness_probe LIVENESS_PROBE] [--retry RETRY] [--debug]

OpenTestFactory Agent

optional arguments:
  -h, --help            show this help message and exit
  --tags TAGS           a comma-separated list of tags (e.g. windows,robotframework)
  --host HOST           target host with protocol (e.g. https://example.local)
  --port PORT           target port (default to 24368)
  --path_prefix PATH_PREFIX
                        target context path (default to no context path)
  --token TOKEN         token
  --encoding ENCODING   encoding on the console side (defaults to utf-8)
  --script_path SCRIPT_PATH
                        where to put temporary files (defaults to current directory)
  --workspace_dir WORKSPACE_DIR
                        where to put workspaces (defaults to current directory)
  --name NAME           agent name (defaults to "test agent")
  --polling_delay POLLING_DELAY
                        polling delay in seconds (default to 5)
  --liveness_probe LIVENESS_PROBE
                        liveness probe in seconds (default to 300 seconds)
  --retry RETRY         how many time to try joining host (default to 5,
                        0 = try forever)
  --debug               whether to log debug informations.
```

#### Exemple

En considérant qu'un **Squash Orchestrator** est lancé sur ``orchestrator.example.com``, avec un jeton stocké dans la variable d'environnement ``TOKEN``, la commande suivante enregistre l'environnement d'exécution Windows et recevra les commandes ciblant windows et/ou robotframework :

```bash
chcp 65001
opentf-agent --tags windows,robotframework --host http://orchestrator.example.com/ --token %TOKEN%
```

L'agent contactera l'orchestrateur toutes les 5 secondes, et exécutera les commandes receptionnées.  
La commande ``chcp`` configure la console en Unicode. Il s'agit d'une spécificité Windows. Cette configuration peut être nécessaire suivant le framework de test disponible sur l'environnement d'exécution.
