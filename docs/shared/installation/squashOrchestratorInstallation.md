## Squash Orchestrator†


### Installation

**Squash Orchestrator** se présente sous la forme d'un ensemble de services fonctionnant ensemble. Ils peuvent ou non être lancés sur une même machine, et être ou non démarrés simultanément.

Le seul prérequis est que le service *EventBus*, qui permet la communication entre les différents micro-services, soit disponible au moment du lancement des autres micro-services.

Pour faciliter l'installation de **Squash Orchestrator**, une image docker "all-in-one" est mise à disposition. Elle contient
l'ensemble des services nécessaires de l'**OpenTestFactory Orchestrator** et des services spécifiques Squash.

Pour récupérer la dernière version de l'image de **Squash Orchestrator**, la commande suivante est à exécuter :

    docker pull squashtest/squash-orchestrator:latest

Les préconisations pour dimensionner le conteneur Docker sont :

* 2 CPU
* RAM : 1 Go (2 Go recommandé)
* disque : prévoir la capacité de stocker les rapports d’exécution et leurs attachements pour une heure d'utilisation

### Usage

#### Configuration de l'image

La commande suivante permet de démarrer **Squash Orchestrator** en lui permettant d'utiliser un environnement d'exécution existant, avec auto-génération d'une clé approuvée (ce qui n'est **pas** recommandé dans un environnement de production).

``` bash
docker run -d \
         --name orchestrator \
         -p 7774:7774 \
         -p 7775:7775 \
         -p 7776:7776 \
         -p 38368:38368 \
         -e SSH_CHANNEL_HOST=the_environment_ip_or_hostname \
         -e SSH_CHANNEL_USER=user \
         -e SSH_CHANNEL_PASSWORD=secret \
         -e SSH_CHANNEL_TAGS=ssh,linux,robotframework \
         squashtest/squash-orchestrator:latest
```

Cette commande expose les services suivants sur les ports correspondants :

* *receptionnist* (port 7774)
* *observer* (port 7775)
* *killswitch* (port 7776)
* *eventbus* (port 38368)

### Lancement de l'image en mode Premium

Si vous disposez d'une licence **Squash AUTOM Premium** et avez déployé la version **Premium** du plugin Squash TM *Result Publisher*, vous devez démarrer **Squash Orchestrator** en mode **Premium** pour profiter du comportement **Premium** pour la remontée de résultats vers **Squash TM**. Pour cela, il faut ajouter le paramètre suivant dans la commande de démarrage de **Squash Orchestrator** : ``-e SQUASH_LICENCE_TYPE=premium``

Pour une configuration plus complète de  **Squash Orchestrator**, vous pouvez vous reporter à la <a href="https://opentestfactory.gitlab.io/orchestrator/installation/" target="_blank">documentation de l'OpenTestFactory</a> qui sert de base au **Squash Orchestrator**.
