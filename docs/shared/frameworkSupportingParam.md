!!! info "Info"
    Les technologies suivantes supportent l'exploitation des paramètres Squash TM au sein des cas de test :

    * [Agilitest](../autom/agilitest.md)
    * [Cucumber](../autom/cucumber.md)
    * [Cypress](../autom/cypress.md)
    * [JUnit](../autom/junit.md)
    * [Postman](../autom/postman.md)
    * [Ranorex](../autom/ranorex.md)
    * [Robot Framework](../autom/robotframework.md)
    * [SKF](../autom/skf.md)
    * [UFT](../autom/uft.md)
