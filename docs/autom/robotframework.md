# Automatisation avec Robot Framework

## Environment de test

Le module Python `allure-robotframework` doit être présent dans l'environnement de test.

Celui-ci peut être installé grâce à la commande :
```bash
pip3 install allure-robotframework
```

## Référence du test dans Squash TM

Pour lier un cas de test **Squash TM** à un test automatisé, le champ *Référence du test automatisé* du bloc *Automatisation* du cas de test doit avoir une forme spécifique au framework de test utilisé :

``[1] / [2] # [3]``

Avec :

* ``[1]`` : Nom du projet sur le dépôt de source.

* ``[2]`` : Chemin et nom du fichier de test *Robot Framework* à partir de la racine du projet (avec son extension ``.robot``).

* ``[3]`` : Nom du cas de test à exécuter dans le fichier ``.robot``.

## Nature des paramètres Squash TM exploitables

Les paramètres **Squash TM** exploitables dans un script *Robot Framework* vont différer suivant si vous utilisez les composants **Squash DEVOPS Community** ou **Squash DEVOPS Premium**.

Voici le tableau des paramètres exploitables :

| Nature                                  | Clé            | Community               | Premium                 |
| :-------------------------------------: | :------------: | :---------------------: | :---------------------: |
| Nom du jeu de données                   | DSNAME         | ![OK](resources/ok.jpg) | ![OK](resources/ok.jpg) |
| Paramètre d'un jeu de données           | DS_[nom]       | ![OK](resources/ok.jpg) | ![OK](resources/ok.jpg) |
| Référence du cas de test                | TC_REF         | ![OK](resources/ok.jpg) | ![OK](resources/ok.jpg) |
| Champ personnalisé du cas de test       | TC_CUF_[code]  | ![OK](resources/ok.jpg) | ![OK](resources/ok.jpg) |
| Champ personnalisé de l'itération       | IT_CUF_[code]  | ![KO](resources/ko.jpg) | ![OK](resources/ok.jpg) |
| Champ personnalisé de la campagne       | CPG_CUF_[code] | ![KO](resources/ko.jpg) | ![OK](resources/ok.jpg) |
| Champ personnalisé de la suite de tests | TS_CUF_[code]  | ![KO](resources/ko.jpg) | ![OK](resources/ok.jpg) |

*Légende :*

* ``[code]`` : *valeur renseignée dans le champ “Code” d’un champ personnalisé*
* ``[nom]`` : *nom du paramètre tel que renseigné dans Squash TM*

## Utilisation de paramètres Squash TM

Il est possible lors de l’exécution d’un cas de test **Squash TM** automatisé avec *Robot Framework* d’exploiter des paramètres **Squash TM** au sein de celui-ci.

Pour cela, il faut suivre les étapes suivantes :

* Renseigner des champs personnalisés dans **Squash TM** et les associer au projet portant le plan de tests à exécuter.

* Installer sur le/les environnement(s) d’exécution *Robot Framework* la librairie python *squash-tf-services*. Elle est accessible par le gestionnaire de package ``pip`` et peut s’installer en exécutant la ligne de commande suivante :

``` bash
python -m pip install squash-tf-services
```

* Importer la librairie au sein du fichier ``.robot`` dans la section *Settings* :

``` bash
Library squash_tf.TFParamService
```

* Vous pouvez ensuite récupérer la valeur d’un paramètre **Squash TM** en faisant appel au mot-clef suivant :

``` bash
Get Param <clé du paramètre>
```

Ci-dessous un exemple de fichier de test *Robot Framework* et l’automatisation du cas de test **Squash TM** associé :

=== "Squash TM 1.22.X"

    ![robot-params-1](resources/robot-test-with-param-1.png){class="center"}

    ![robot-params-2](resources/robot-test-with-param-2.png){class="center"}

=== "Squash TM 2.X.Y"

    ![robot-params-1](resources/robot-test-with-param-1.png){class="center"}

    ![robot-params-2](resources/robot-test-with-param-2-TM2.png){class="center"}

## Versions supportées

**Squash AUTOM** et **Squash DEVOPS** ont été validés avec Robot Framework 4.0. Toute version récente devrait fonctionner.
