# Pilotage de l’exécution de tests automatisés depuis Squash TM

## Automatisation d’un cas de test Squash TM

!!! info "Info"
    Cette page décrit les opérations communes à tous les frameworks de test supportés par cette version.
    Pour faciliter la navigation vous pouvez directement consulter les spécificités de l'automatisation de chaque technologie grâce aux liens suivants :

    * [Agilitest](agilitest.md)
    * [Cucumber](cucumber.md)
    * [Cypress](cypress.md)
    * [JUnit](junit.md)
    * [Postman](postman.md)
    * [Ranorex](ranorex.md)
    * [Robot Framework](robotframework.md)
    * [SKF](skf.md)
    * [SoapUI](soapui.md)
    * [UFT](uft.md)

### Sans utilisation de workflow d’automatisation Squash

Pour qu’un cas de test soit utilisable par **Squash Orchestrator**, il faut que le bloc *Automatisation* de l’onglet *Information* de la page de consultation d’un cas de test soit correctement renseigné :

=== "Squash TM 1.22.X"

    ![Espace automatisation](resources/tc-automated-space.png){class="center"}

=== "Squash TM 2.X.Y"

    ![Espace automatisation](resources/tc-automated-space-TM2.png){class="center"}

* ``Technologie du test automatisé`` : Liste déroulante permettant de choisir la technologie utilisée pour exécuter le cas de test.

* ``URL du dépôt de code source`` : L’adresse du dépôt de source où se trouve le projet, tel que spécifié dans l'espace *Serveurs de partage de code source* de l’*Administration*.

* ``Référence du test automatisé`` : Correspond à l’emplacement du test automatisé dans le projet. Cette référence doit respecter un format propre à la technologie de test employée (voir [ici](#specificites-pour-lautomatisation-suivant-le-framework-dautomatisation)).

!!! info "Info"
    *Agilitest*, *Ranorex* et *UFT* ne sont supportés que par la version *Premium* de **Squash AUTOM**.

------------------------------------------------------------------------

### Avec utilisation de workflow d’automatisation Squash

#### Cas de test classique

Pour qu’un cas de test soit utilisable par **Squash Orchestrator**, il doit être automatisé à partir de l’espace *Automatisation (Automaticien)* via trois colonnes à renseigner :

=== "Squash TM 1.22.X"

    ![Espace automatisation](resources/autom-workspace-columns.png){class="center"}

    * ``Tech. test auto.`` : Liste déroulante permettant de choisir la technologie utilisée pour exécuter le cas de test.

    * ``URL du SCM`` : L’adresse du dépôt de source où se trouve le projet.

    * ``Ref. test auto.`` : Correspond à l’emplacement du test automatisé dans le projet. Cette référence doit respecter un format propre à la technologie de test employée (voir [ici](#specificites-pour-lautomatisation-suivant-le-framework-dautomatisation)).

=== "Squash TM 2.X.Y"

    ![Espace automatisation](resources/autom-workspace-columns-TM2.png){class="center"}

    * ``TECHNO.`` : Liste déroulante permettant de choisir la technologie utilisée pour exécuter le cas de test.

    * ``URL SCM`` : L’adresse du dépôt de source où se trouve le projet.

    * ``REF. TEST AUTO`` : Correspond à l’emplacement du test automatisé dans le projet. Cette référence doit respecter un format propre à la technologie de test employée (voir [ici](#specificites-pour-lautomatisation-suivant-le-framework-dautomatisation)).

!!! info "Info"
    *Agilitest*, *Ranorex* et *UFT* ne sont supportés que par la version *Premium* de **Squash AUTOM**.

#### Cas de test BDD ou Gherkin

Les informations du bloc *Automatisation* sont remplis automatiquement lors de la transmission d’un script BDD ou Gherkin à un gestionnaire de code source distant. Ils sont également modifiables à tout moment par l’utilisateur.

------------------------------------------------------------------------

### Exploitation de paramètres Squash TM

Au lancement d’un plan d’exécution **Squash TM** (via un PEaC ou directement depuis l'espace campagne), celui-ci transmet différentes informations sur les éléments du plan d'exécution qu’il est possible d’exploiter dans un cas de test. Les détails de cette fonctionnalité sont décrits dans la section correspondant à la technologie utilisée.

--8<--
shared/frameworkSupportingParam.md
--8<--

------------------------------------------------------------------------

## Exécution d'une suite automatisée

L'exécution d'un plan d'exécution automatisée se déroule de la façon habituelle dans **Squash TM** :

* Accédez au plan d'exécution de l'itération ou de la suite de test choisie.

* Exécutez les tests automatisés en utilisant un des boutons de l'image ci-dessous :

=== "Squash TM 1.22.X"

    ![test-plan](resources/test-plan.png){class="fullpage"}

=== "Squash TM 2.X.Y"

    ![test-plan](resources/test-plan-TM2.png){class="fullpage"}

* Une popup de suivi d'exécution apparaît.

!!! info "Info"
    La popup de suivi contient une nouvelle section qui rappelle le nom des tests en cours d'exécution par **Squash Orchestrator**. Cependant il n'y a pas de suivi d'avancement dans la version actuelle.

## Remontées de résultats après exécution d’un plan d’exécution Squash TM

Quelle que soit la façon dont le plan d’exécution est déclenché (depuis **Squash TM** ou depuis un pipeline *Jenkins*), vous avez en fin d’exécution une remontée de résultats au sein de **Squash TM** qui diffère suivant si vous êtes sous licence *Community* ou *Premium*.

------------------------------------------------------------------------

### Squash AUTOM Community

Après exécution d’un plan d’exécution **Squash TM** (itération ou suite de tests), les informations suivantes sont mises à jour :

* Mise à jour du statut des ITPI  (Iteration Test Plan Item, voir le [glossaire Squash TM](https://tm-fr.doc.squashtest.com/introduction/#terminologie)).

* Mise à jour du statut de la suite automatisée.

* Le rapport au format *Allure* correspondant à l'ensemble des tests exécutés.

* Les rapports d’exécution des différents ITPI sont accessibles depuis l’onglet de consultation des suites automatisées :

=== "Squash TM 1.22.X"

    ![automated-suite-tab](resources/automated-suite-tab.png){class="fullpage"}

=== "Squash TM 2.X.Y"

    ![automated-suite-tab](resources/automated-suite-tab-TM2.png){class="fullpage"}

!!! info "Info"
    Tous les résultats de la suite automatisée sont compilés dans un rapport au format *Allure*, disponible dans la liste de rapports sous forme d'archive *.tar*.

    Pour plus d'information sur la façon d'exploiter ce rapport et les possibilités de personnalisation, veuillez consulter la [documentation Allure](https://docs.qameta.io/allure/).

Cependant, voici ce qu’il ne se passe pas :

* Création d’une nouvelle exécution pour chaque ITPI qui a été exécuté.

------------------------------------------------------------------------

### Squash AUTOM Premium

Si vous disposez des composants **Squash AUTOM Premium**, vous avez accès à deux types de remontées d’informations :

* Légère (valeur par défaut).

* Complète.

Le choix du type de remontée se fait par projet en accédant à la configuration du plugin **Squash TM Result Publisher** depuis l’onglet *Plugins* de la page de consultation d’un projet :

=== "Squash TM 1.22.X"

    ![plugin-configuration](resources/plugin-complete-switch.png){class="center"}

=== "Squash TM 2.X.Y"

    ![plugin-configuration](resources/plugin-complete-switch-TM2.png){class="center"}

#### Remontée d’information Légère

En choisissant la remontée d’information *Légère*, les informations suivantes sont mises à jour après exécution d’un plan d’exécution **Squash TM** (itération ou suite de tests) :

* Mise à jour du statut des ITPI.

* Mise à jour du statut de la suite automatisée.

* Le rapport au format *Allure* correspondant à l'ensemble des tests exécutés.

* Les rapports d’exécution des différents ITPI sont accessibles depuis l’onglet de consultation des suites automatisées :

=== "Squash TM 1.22.X"

    ![automated-suite-tab](resources/automated-suite-tab.png){class="fullpage"}

=== "Squash TM 2.X.Y"

    ![automated-suite-tab](resources/automated-suite-tab-TM2.png){class="fullpage"}

!!! info "Info"
    Tous les résultats de la suite automatisée sont compilés dans un rapport au format *Allure*, disponible dans la liste de rapports sous forme d'archive *.tar*.  
    Pour plus d'information sur la façon d'exploiter ce rapport et les possibilités de personnalisation, veuillez consulter la <a href="https://docs.qameta.io/allure/" target="_blank">documentation Allure</a>.

Cependant, voici ce qu’il ne se passe pas :

* Création d’une nouvelle exécution pour chaque ITPI qui a été exécuté.

#### Remontée d’information Complète

En choisissant la remontée d’information *Complète*, les informations suivantes sont mises à jour après exécution d’un plan d’exécution **Squash TM** (itération ou suite de tests) :

* Mise à jour du statut des ITPI.

* Création d’une nouvelle exécution pour chaque ITPI.

* Mise à jour du statut de la suite automatisée.

* Le rapport au format *Allure* correspondant à l'ensemble des tests exécutés.

* Les rapports d’exécution des différentes exécutions sont accessibles depuis l’onglet de consultation des suites automatisées ou depuis l’écran de consultation de l’exécution (ils sont présents dans les pièces jointes).

=== "Squash TM 1.22.X"

    ![iteration-execution-tab](resources/iteration-execution-tab.png){class="fullpage"}

    ![iteration-execution-detail](resources/iteration-execution-detail.png){class="fullpage"}

=== "Squash TM 2.X.Y"

    ![iteration-execution-tab](resources/iteration-execution-tab-TM2.png){class="fullpage"}

    ![iteration-execution-detail](resources/iteration-execution-detail-TM2.png){class="fullpage"}

!!! info "Info"
    Tous les résultats de la suite automatisée sont compilés dans un rapport au format *Allure*, disponible dans la liste de rapports sous forme d'archive *.tar*.  
    Pour plus d'information sur la façon d'exploiter ce rapport et les possibilités de personnalisation, veuillez consulter la <a href="https://docs.qameta.io/allure/" target="_blank">documentation Allure</a>.
