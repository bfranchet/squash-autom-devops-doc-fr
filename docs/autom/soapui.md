# Automatisation avec SoapUI

## Référence du test dans Squash TM

Pour lier un cas de test **Squash TM** à un test automatisé *SoapUI*, le champ *Référence du test automatisé* du bloc *Automatisation* du cas de test doit avoir la forme suivante :

``[1] / [2] # [3] # [4]``

Avec :

* ``[1]`` : Nom du projet sur le dépôt de source.

* ``[2]`` : Chemin et nom du fichier de test *SoapUI* à partir de la racine du projet (avec son extension ``.xml``).

* ``[3]`` : Nom de la Suite de test *SoapUI* contenant le cas de test.

* ``[4]`` : Nom du cas de test à exécuter.

Ci-dessous un exemple de fichier de test *SoapUI* et l’automatisation du cas de test **Squash TM** associé :

=== "Squash TM 1.22.X"

    ![soapui-1](resources/soapui-test-1.png){class="fullpage"}

    ![soapui-2](resources/soapui-test-2.png){class="fullpage"}

=== "Squash TM 2.X.Y"

    ![soapui-1](resources/soapui-test-1.png){class="fullpage"}

    ![soapui-2](resources/soapui-test-2-TM2.png){class="fullpage"}

## Versions supportées

**Squash AUTOM** et **Squash DEVOPS** ont été validés avec SoapUI 5.6.0. Toute version récente devrait fonctionner.

