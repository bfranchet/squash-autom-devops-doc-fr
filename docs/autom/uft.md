# Automatisation avec UFT

Cette fonctionnalité est disponible uniquement dans la version *Premium* de **Squash AUTOM**.

!!! danger "Attention"
    Les Service Tests UFT ne sont pas supportés dans la version actuelle de **Squash AUTOM**/**Squash DEVOPS**.

!!! warning "Focus"
    Pour la remontée d'information vers Squash TM, UFT doit être configuré pour générer un rapport au format RRV.

    ![uft-report-format](resources/uft-report-format.png){class="center"}

## Référence du test dans Squash TM

Pour lier un cas de test **Squash TM** à un test automatisé, le champ *Référence du test automatisé* du bloc *Automatisation* du cas de test doit avoir une forme spécifique au framework de test utilisé :

``[1]``

Avec :

* ``[1]`` : Chemin vers le dossier du test à exécuter.

## Nature des paramètres Squash TM exploitables

Voici le tableau des paramètres exploitables :

| Nature                                  | Clé            |
| :-------------------------------------: | :------------: |
| Nom du jeu de données                   | DSNAME         |
| Paramètre d'un jeu de données           | DS_[nom]       |
| Référence du cas de test                | TC_REF         |
| Champ personnalisé du cas de test       | TC_CUF_[code]  |
| Champ personnalisé de l'itération       | IT_CUF_[code]  |
| Champ personnalisé de la campagne       | CPG_CUF_[code] |
| Champ personnalisé de la suite de tests | TS_CUF_[code]  |

*Légende :*

* ``[code]`` : *valeur renseignée dans le champ “Code” d’un champ personnalisé*
* ``[nom]`` : *nom du paramètre tel que renseigné dans Squash TM*

## Utilisation de paramètres Squash TM

Il est possible lors de l’exécution d’un cas de test **Squash TM** automatisé avec *UFT* d’exploiter des paramètres **Squash TM** au sein de celui-ci.

Pour cela, il faut suivre les étapes suivantes :

* Renseigner des champs personnalisés dans **Squash TM** et les associer au projet portant le plan de tests à exécuter.

* S'assurer que les champs *code* des paramètres correspondent aux noms des paramètres existants dans le test *UFT*.

Ci-dessous un exemple de projet *UFT* et l’automatisation du cas de test **Squash TM** associé :

=== "Squash TM 1.22.X"

    ![uft-params-1](resources/uft-test-with-params-1.png){class="center"}

    ![uft-params-2](resources/uft-test-with-params-2.png){class="center"}

=== "Squash TM 2.X.Y"

    ![uft-params-1](resources/uft-test-with-params-1.png){class="center"}

    ![uft-params-2](resources/uft-test-with-params-2-TM2.png){class="center"}

## Versions supportées

**Squash AUTOM** et **Squash DEVOPS** ont été validés avec UFT 15.0.2. Toute version récente devrait fonctionner.
