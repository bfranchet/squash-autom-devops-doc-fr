# Pilotage de l’exécution de tests automatisés via un PEaC (Plan d’Exécution «as Code»)

L'**OpenTestFactory Orchestrator**, sur lequel est basé **Squash AUTOM**, interprète des plans d’exécution dans un formalisme spécifique, les PEaC (Plan d’Exécution «as Code»), pour orchestrer précisément l’exécution des tests automatisés en dehors d’un référentiel de test.

Un PEaC se présente sous la forme d'un fichier `YAML` ou `JSON` décrivant un plan d'exécution de tests automatisés.

Retrouvez plus d’informations sur la rédaction d’un PEaC dans <a href="https://opentestfactory.gitlab.io/orchestrator/reference/workflows/" target="_blank">la documentation de l'**OpenTestFactory Orchestrator**</a>.
