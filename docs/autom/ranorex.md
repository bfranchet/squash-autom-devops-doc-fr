# Automatisation avec Ranorex

Cette fonctionnalité est disponible uniquement dans la version *Premium* de **Squash AUTOM**.

## Configuration de l'environnement de test

Dans l'environnement d'exécution, le chemin vers le dossier contenant **MSBuild.exe** doit être renseigné dans une variable d'environnement nommée **SQUASH_MSBUILD_PATH**. Vous pouvez exécuter la commande suivante pour trouver le chemin vers votre exécutable *MSBuild* :

    reg.exe query "HKLM\SOFTWARE\Microsoft\MSBuild\ToolsVersions\4.0" /v MSBuildToolsPath

## Référence du test dans Squash TM

Pour lier un cas de test **Squash TM** à un test automatisé, le champ *Référence du test automatisé* du bloc *Automatisation* du cas de test doit avoir une forme spécifique au framework de test utilisé :

``[1] / [2] # [3] # [4] # [5]``

Avec :

* ``[1]`` : Nom du projet sur le dépôt de source.

* ``[2]`` : Chemin vers le fichier ``.sln`` de la solution à partir de la racine du projet sur le dépôt de source.

* ``[3]`` : Nom du projet *Ranorex* à exécuter.

* ``[4]`` : Nom de la suite de tests *Ranorex* à exécuter.

* ``[5]`` : Nom du cas de test *Ranorex* à exécuter. Ce paramètre est optionnel.

## Nature des paramètres Squash TM exploitables

Voici le tableau des paramètres exploitables :

| Nature                                  | Clé            |
| :-------------------------------------: | :------------: |
| Nom du jeu de données                   | DSNAME         |
| Paramètre d'un jeu de données           | DS_[nom]       |
| Référence du cas de test                | TC_REF         |
| Champ personnalisé du cas de test       | TC_CUF_[code]  |
| Champ personnalisé de l'itération       | IT_CUF_[code]  |
| Champ personnalisé de la campagne       | CPG_CUF_[code] |
| Champ personnalisé de la suite de tests | TS_CUF_[code]  |

*Légende :*

* ``[code]`` : *valeur renseignée dans le champ “Code” d’un champ personnalisé*
* ``[nom]`` : *nom du paramètre tel que renseigné dans Squash TM*

## Utilisation de paramètres Squash TM

Il est possible lors de l’exécution d’un cas de test **Squash TM** automatisé avec *Ranorex* d’exploiter des paramètres **Squash TM** au sein de celui-ci.

Pour cela, il faut suivre les étapes suivantes :

* Renseigner des champs personnalisés dans **Squash TM** et les associer au projet portant le plan de tests à exécuter.

* S'assurer que les champs *code* des paramètres correspondent aux noms des paramètres existants dans la
  solution *Ranorex*.

!!! info "Info"
    **Squash TM** ajoute un préfixe au *code* du champ personnalisé transmis. Assurez-vous de le prendre en compte.  
    Voir la [documentation](https://tm-fr.doc.squashtest.com/admin-guide/personnalisation-entites/gerer-champs-persos/) de **Squash TM** pour plus d'information.

Ci-dessous un exemple de solution *Ranorex* et l’automatisation du cas de test **Squash TM** associé :

=== "Squash TM 1.22.X"

    ![ranorex-params-1](resources/ranorex-test-with-param-1.png){class="fullpage"}

    ![ranorex-params-2](resources/ranorex-test-with-param-2.png){class="fullpage"}

=== "Squash TM 2.X.Y"

    ![ranorex-params-1](resources/ranorex-test-with-param-1.png){class="fullpage"}

    ![ranorex-params-2](resources/ranorex-test-with-param-2-TM2.png){class="fullpage"}

## Versions supportées

**Squash AUTOM** et **Squash DEVOPS** ont été validés avec Ranorex 9.5. Toute version récente devrait fonctionner.
