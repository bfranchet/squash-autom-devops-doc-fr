# Automatisation avec Agilitest


Cette fonctionnalité est disponible uniquement dans la version *Premium* de **Squash AUTOM**.

## Référence du test dans Squash TM

Pour lier un cas de test **Squash TM** à un test automatisé *Agilitest*, le champ *Référence du test automatisé* du bloc *Automatisation* du cas de test doit avoir la forme suivante :

``[1] / [2]``

Avec :

* ``[1]`` : Nom du projet sur le dépôt de source.

* ``[2]`` : Chemin et nom du fichier de script *ActionTestScript* à partir de la racine du projet (avec son extension ``.ats``).

!!! warning "Focus"
    Les scripts ATS doivent impérativement se trouver dans une arborescence de fichiers de la forme ``src/main/ats/*`` , conformément à l'architecture classique d'un projet ATS.

## Nature des paramètres Squash TM exploitables

Les paramètres **Squash TM** exploitables dans un script *ActionTestScript* vont différer suivant si vous utilisez les composants **Squash DEVOPS Community** ou **Squash DEVOPS Premium**.

Voici le tableau des paramètres exploitables :

| Nature                                  | Clé            | Community               | Premium                 |
| :-------------------------------------: | :------------: | :---------------------: | :---------------------: |
| Nom du jeu de données                   | DSNAME         | ![OK](resources/ok.jpg) | ![OK](resources/ok.jpg) |
| Paramètre d'un jeu de données           | DS_[nom]       | ![OK](resources/ok.jpg) | ![OK](resources/ok.jpg) |
| Référence du cas de test                | TC_REF         | ![OK](resources/ok.jpg) | ![OK](resources/ok.jpg) |
| Champ personnalisé du cas de test       | TC_CUF_[code]  | ![OK](resources/ok.jpg) | ![OK](resources/ok.jpg) |
| Champ personnalisé de l'itération       | IT_CUF_[code]  | ![KO](resources/ko.jpg) | ![OK](resources/ok.jpg) |
| Champ personnalisé de la campagne       | CPG_CUF_[code] | ![KO](resources/ko.jpg) | ![OK](resources/ok.jpg) |
| Champ personnalisé de la suite de tests | TS_CUF_[code]  | ![KO](resources/ko.jpg) | ![OK](resources/ok.jpg) |

*Légende :*

* ``[code]`` : *valeur renseignée dans le champ “Code” d’un champ personnalisé*
* ``[nom]`` : *nom du paramètre tel que renseigné dans Squash TM*

## Utilisation de paramètres Squash TM

Il est possible lors de l’exécution d’un cas de test **Squash TM** automatisé avec *Agilitest* d’exploiter des paramètres **Squash TM** au sein de celui-ci.

Pour cela, il faut suivre les étapes suivantes :

* Renseigner des champs personnalisés dans **Squash TM** et les associer au projet portant le plan de tests à exécuter.

* S'assurer que les champs *code* des paramètres correspondent aux noms des variables d'environnement existants dans le script *ActionTestScript*.

!!! info "Info"
    **Squash TM** ajoute un préfixe au *code* du champ personnalisé transmis. Assurez-vous de le prendre en compte.  
    Voir la [documentation](https://tm-fr.doc.squashtest.com/admin-guide/personnalisation-entites/gerer-champs-persos/) de **Squash TM** pour plus d'information.

Ci-dessous un exemple d'un fichier de test *Agilitest* et l’automatisation du cas de test **Squash TM** associé :

=== "Squash TM 1.22.X"

    ![agilitest-param-1](resources/agilitest-test-with-params-1.png){class="fullpage"}

    ![agilitest-param-2](resources/agilitest-test-with-params-2.png){class="fullpage"}

=== "Squash TM 2.X.Y"

    ![agilitest-param-1](resources/agilitest-test-with-params-1.png){class="fullpage"}

    ![agilitest-param-2](resources/agilitest-test-with-params-2-TM2.png){class="fullpage"}

## Versions supportées

**Squash AUTOM** et **Squash DEVOPS** ont été validés avec ATS 2.1.4. Toute version récente devrait fonctionner.
