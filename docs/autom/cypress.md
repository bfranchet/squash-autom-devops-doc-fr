# Automatisation avec Cypress

## Référence du test dans Squash TM

!!! info "Info"
    Dans cette version de **Squash AUTOM** il n'est pas possible de sélectionner un test spécifique dans un fichier ``.spec.js`` qui en contiendrait plusieurs : tous les tests du fichier sont donc exécutés ensemble.  
    Le résultat de chaque exécution du cas de test **Squash TM** est calculé en prenant en compte les résultats individuels de chaque test inclus dans le fichier lié :

    * Si au moins un test est en statut *Error* (dans le cas d'un problème technique), l'exécution sera en statut *Blocked*.
    * Si au moins un test a échoué fonctionnellement et qu'aucun test n'est en statut *Error*, l'exécution sera en statut *Failed*.
    * Si tous les tests ont réussi, l'exécution sera en statut *Success*.

Pour lier un cas de test **Squash TM** à un test automatisé *Cypress*, le champ *Référence du test automatisé* du bloc *Automatisation* du cas de test doit avoir la forme suivante :

``[1] / [2]``

Avec :

* ``[1]`` : Nom du projet sur le dépôt de source.

* ``[2]`` : Chemin et nom du fichier de test *Cypress* à partir de la racine du projet (avec son extension ``.spec.js``).

## Nature des paramètres Squash TM exploitables

Les paramètres **Squash TM** exploitables dans un script *Cypress* vont différer suivant si vous utilisez les composants **Squash DEVOPS Community** ou **Squash DEVOPS Premium**.

Voici le tableau des paramètres exploitables :

| Nature                                  | Clé            | Community               | Premium                 |
| :-------------------------------------: | :------------: | :---------------------: | :---------------------: |
| Nom du jeu de données                   | DSNAME         | ![OK](resources/ok.jpg) | ![OK](resources/ok.jpg) |
| Paramètre d'un jeu de données           | DS_[nom]       | ![OK](resources/ok.jpg) | ![OK](resources/ok.jpg) |
| Référence du cas de test                | TC_REF         | ![OK](resources/ok.jpg) | ![OK](resources/ok.jpg) |
| Champ personnalisé du cas de test       | TC_CUF_[code]  | ![OK](resources/ok.jpg) | ![OK](resources/ok.jpg) |
| Champ personnalisé de l'itération       | IT_CUF_[code]  | ![KO](resources/ko.jpg) | ![OK](resources/ok.jpg) |
| Champ personnalisé de la campagne       | CPG_CUF_[code] | ![KO](resources/ko.jpg) | ![OK](resources/ok.jpg) |
| Champ personnalisé de la suite de tests | TS_CUF_[code]  | ![KO](resources/ko.jpg) | ![OK](resources/ok.jpg) |

*Légende :*

* ``[code]`` : *valeur renseignée dans le champ “Code” d’un champ personnalisé*
* ``[nom]`` : *nom du paramètre tel que renseigné dans Squash TM*

## Utilisation de paramètres Squash TM

Il est possible lors de l’exécution d’un cas de test **Squash TM** automatisé avec *Cypress* d’exploiter des paramètres **Squash TM** au sein de celui-ci.

Pour cela, il faut suivre les étapes suivantes :

* Renseigner des champs personnalisés dans **Squash TM** et les associer au projet portant le plan de tests à exécuter.

* S'assurer que les champs *code* des paramètres correspondent aux noms des variables d'environnement existants dans le script *Cypress*.

!!! info "Info"
    **Squash TM** ajoute un préfixe au *code* du champ personnalisé transmis. Assurez-vous de le prendre en compte.  
    Voir la [documentation](https://tm-fr.doc.squashtest.com/admin-guide/personnalisation-entites/gerer-champs-persos/) de **Squash TM** pour plus d'information.

Ci-dessous un exemple d'un fichier de test *Cypress* et l’automatisation du cas de test **Squash TM** associé :

=== "Squash TM 1.22.X"

    ![cypress-params-1](resources/cypress-test-with-params-1.png){class="fullpage"}

    ![cypress-params-2](resources/cypress-test-with-params-2.png){class="fullpage"}

=== "Squash TM 2.X.Y"

    ![cypress-params-1](resources/cypress-test-with-params-1.png){class="fullpage"}

    ![cypress-params-2](resources/cypress-test-with-params-2-TM2.png){class="fullpage"}

## Génération des rapports *Allure* avec l'action *cypress/cypress@v1*

Dans le cas de l'**utilisation de l'<a href="https://opentestfactory.gitlab.io/orchestrator/providers/cypress/#cypresscypressv1" target="_blank">action *cypress/cypress@v1*</a>**‡ (via le [pilotage d'exécution via un PEaC](pilotWithPeac.md)), si vous voulez que les résultats du test *Cypress* soient pris en compte dans le rapport *Allure* généré pour le PEaC, vous devez : 

1) configurer (dans le fichier de configuration `cypress.yaml` du provider *cypress*) un <a href="https://opentestfactory.gitlab.io/orchestrator/guides/hooks/" target="_blank">hook</a> pour remonter les rapports JUnit :
<pre><code>apiVersion: opentestfactory.org/v1alpha1
kind: ServiceConfig
current-context: allinone
contexts:
- context:
    port: 7789
    host: 127.0.0.1
    ssl_context: disabled
    trusted_authorities:
    - /etc/squashtf/squashtf.pub
    logfile: cypress.log
    enable_insecure_login: true
    eventbus:
        endpoint: http://127.0.0.1:38368
        token: reuse
  name: allinone
<b>hooks:
- name: capture JUnit reports
  events:
  - categoryPrefix: cypress
    category: cypress
  after:
  - uses: actions/get-files
    with:
      pattern: test-output-*.xml</b></code></pre>

2) indiquer dans le PEaC de générer un rapport JUnit par test avec les valeurs suivantes pour `reporter` et `reporter-options` :
<pre><code>{
  "apiVersion": "opentestfactory.org/v1alpha1",
  "kind": "Workflow",
  "metadata": {
    "name": "Cypress"
  },
  "jobs": {
    "execute": {
      "runs-on": "cypress",
      "steps": [
        {
          "uses": "actions/checkout@v2",
          "with": {
            "repository": "https://github.com/my-repo"
          }
        },
        {
          "uses": "cypress/cypress@v1",
          "with": {
            "reporter": "junit",
            <b>"reporter-options": "mochaFile=test-output-[hash].xml,toConsole=true",
            "headless": "true"</b>
          },
          "working-directory": "cypressDocCheck"
        }
      ]
    }
  }
}</code></pre>

‡ Ceci n'est pas nécessaire pour les tests *Cypress* lancés [depuis Squash TM](pilotFromSquash.md) ou via [la récupération d'un plan d'exécution Squash TM depuis un PEaC](../devops/callWithPeac.md).

## Versions supportées

**Squash AUTOM** et **Squash DEVOPS** ont été validés avec Cypress 8.5.0. Toute version récente devrait fonctionner.
