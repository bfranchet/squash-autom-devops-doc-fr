# Automatisation avec Cucumber

!!! info "Compatibilité avec Cucumber 5 ou postérieur"
    Si votre projet intègre une version de Cucumber inférieure à Cucumber 5, sélectionnez la technologie de test **Cucumber 4**. S'il intègre une version supérieure ou égale à Cucumber 5, sélectionnez la technologie de test **Cucumber 5+**.

## Référence du test dans Squash TM

En fonction de la précision du rapport de test que vous souhaitez, vous pouvez affiner votre référence de test à une feature (si votre fichier *.feature* contient plusieurs features, ce qui n'est pas conseillé mais est rendu possible par Cucumber) ou à un scénario en particulier.

Pour lier un cas de test **Squash TM** à un test automatisé *Cucumber*, le champ *Référence du test automatisé* du bloc *Automatisation* du cas de test doit avoir la forme suivante :

``[1] / [2] # [3] # [4]``

Avec :

* ``[1]`` : Nom du projet sur le dépôt de source.

* ``[2]`` : Chemin et nom du fichier de test *Cucumber* à partir de la racine du projet (avec son extension ``.feature``).

* ``[3]`` : Nom de la feature tel que renseigné dans le fichier de test *Cucumber*. Ce paramètre est optionnel.

* ``[4]`` : Nom du scénario tel que renseigné dans le fichier de test *Cucumber*. Ce paramètre est optionnel.

**Exemple** : `MonDepot/chemin/vers/mon/test.feature#Nom de ma feature#Nom de mon scénario`

!!! warning "Référence du test automatisé et exécution"
    La précision de la feature [3] et du scénario [4] dans la référence du test n'influe pas sur l'exécution, mais sur la remontée des données dans le rapport de test.  
    Ainsi, même en définissant une granularité à l'échelle d'une feature ou d'un scénario, **la totalité des tests contenus dans le fichier *.feature* sera exécutée** (ce qui signifie, par exemple, que si plusieurs cas de test Squash TM pointent vers le même fichier mais vers des features ou scénarios différents, tous les tests du fichier seront exécutés plusieurs fois).  
    Les rapports de test contiennent les traces de tous les scénarios exécutés. Par contre, seuls les statuts *Error* / *Failed* / *Success* correspondant à cette granularité fine seront considérés pour définir le résultat du cas de test.

!!! info "Info"
    Dans le cas où un nom de scénario n'est pas spécifié, le résultat de chaque exécution du cas de test **Squash TM** est calculé en prenant en compte les résultats individuels de chaque scénario inclus dans le fichier lié :

    * Si au moins un scénario est en statut *Error* (dans le cas d'un problème technique), l'exécution sera en statut *Blocked*.
    * Si au moins un scénario a échoué fonctionnellement et qu'aucun scénario n'est en statut *Error*, l'exécution sera en statut *Failed*.
    * Si tous les scénarios ont réussi, l'exécution sera en statut *Success*.

## Nature des paramètres Squash TM exploitables

**Squash AUTOM** et **Squash DEVOPS** sont capables d'exploiter le nom d'un jeu de données Squash TM d'un cas de test comme valeur d'un tag à utiliser pour l'exécution d'un sous-ensemble spécifique d'une feature *Cucumber*.

Cette exploitation est possible par les composants en version **Community** ou **Premium**.

## Utilisation de paramètres Squash TM

Il est possible lors de l’exécution d’un cas de test **Squash TM** automatisé avec *Cucumber* d’exploiter un nom de jeu de données **Squash TM** pour n'exécuter qu'un jeu de données particulier d'un scénario *Cucumber*.

Pour cela, il faut suivre les étapes suivantes :

* Renseigner des jeux de données dans l'onglet *Paramètres* du cas de test dans **Squash TM**.

* Créer au sein de son scénario *Cucumber* autant de tableaux exemples que de jeux de données et annoter ces tableaux d'un tag correspondant au nom d'un jeu de données **Squash TM**.

* Créer une seule ligne d'éléments dans chaque tableau exemple afin de fixer une valeur pour les différents paramètres du scénario.

Ci-dessous un exemple d'un fichier de test *Cucumber* et l’automatisation du cas de test **Squash TM** associé :

=== "Squash TM 1.22.X"

    ![cucumber-tags-1](resources/cucumber-test-with-tags-1.png){class="center"}

    ![cucumber-tags-2](resources/cucumber-test-with-tags-2.png){class="fullpage"}

    ![cucumber-tags-3](resources/cucumber-test-with-tags-3.png){class="fullpage"}

=== "Squash TM 2.X.Y"

    ![cucumber-tags-1](resources/cucumber-test-with-tags-1.png){class="center"}

    ![cucumber-tags-2](resources/cucumber-test-with-tags-2-TM2.png){class="fullpage"}

    ![cucumber-tags-3](resources/cucumber-test-with-tags-3-TM2.png){class="fullpage"}

## Génération des rapports *Allure* avec les actions *cucumber/cucumber@v1* et *cucumber5/cucumber@v1*

Dans le cas de l'**utilisation des actions *cucumber/cucumber@v1* et *cucumber5/cucumber@v1*** (voir <a href="https://opentestfactory.gitlab.io/orchestrator/providers/cucumber/" target="_blank">la doc de l'orchestrateur</a>) via le [pilotage d'exécution via un PEaC](pilotWithPeac.md), si vous voulez que les résultats du test *Cucumber* soient pris en compte dans le rapport *Allure* généré pour le PEaC, vous devez utiliser le reporter JUnit (possiblement avec d'autres reporters).

## Versions supportées

**Squash AUTOM** et **Squash DEVOPS** ont été validés avec Cucumber-JVM 4.2.6, 5.7.0 et 6.11.0. Toute version récente devrait fonctionner.
