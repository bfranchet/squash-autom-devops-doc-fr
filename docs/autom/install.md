# Guide d'Installation

--8<--
shared/installation/squashOrchestratorInstallation.md

shared/installation/orchestratorToolsInstallation.md

shared/installation/premiumMicroservices.md

shared/installation/otfAgentInstallation.md

shared/installation/technologySpecifities.md
--8<--

## Plugins Squash TM

### Installation

Pour l’installation des plugins **Squash TM**, merci de vous reporter au [protocole d’installation d’un plugin **Squash TM**](https://tm-fr.doc.squashtest.com/install-guide/installation-plugins/installer-plugins/#installation-des-plugins-squash-tm).

<table>
 <thead>
  <tr>
   <th>Plugin</th>
   <th>Version de Squash TM</th>
   <th>Version compatible du plugin</th>
   <th>Télécharger la dernière version Community</th>
  </tr>
 </thead>
 <tbody>
  <tr>
   <td rowspan="3">Result Publisher†</td>
   <td>1.22.X, à partir de 1.22.2</td>
   <td>1.X.X</td>
   <td>1.0.0 : <a href="https://repo.squashtest.org/distribution/squash.tm.rest.result.publisher.community-1.0.0.RELEASE.tar.gz">.tar.gz</a> ou <a href="https://repo.squashtest.org/distribution/squash.tm.rest.result.publisher.community-1.0.0.RELEASE.zip">.zip</a></td>
  </tr>
  <tr>
   <td>2.0.X</td>
   <td>2.0.X</td>
   <td>2.0.0 : <a href="https://repo.squashtest.org/distribution/squash.tm.rest.result.publisher.community-2.0.0.RELEASE.tar.gz">.tar.gz</a> ou <a href="https://repo.squashtest.org/distribution/squash.tm.rest.result.publisher.community-2.0.0.RELEASE.zip">.zip</a></td>
  </tr>
  <tr>
   <td>2.1.0, ou postérieure</td>
   <td>2.1.X</td>
   <td>2.1.0 : <a href="https://repo.squashtest.org/distribution/squash.tm.rest.result.publisher.community-2.1.0.RELEASE.tar.gz">.tar.gz</a> ou <a href="https://repo.squashtest.org/distribution/squash.tm.rest.result.publisher.community-2.1.0.RELEASE.zip">.zip</a></td>
  </tr>
  <tr>
   <td rowspan="2">Squash AUTOM</td>
   <td>1.22.X, à partir de 1.22.2</td>
   <td>1.X.X</td>
   <td>1.0.3 : <a href="https://repo.squashtest.org/distribution/plugin.testautomation.squashautom.community-1.0.3.RELEASE.tar.gz">.tar.gz</a> ou <a href="https://repo.squashtest.org/distribution/plugin.testautomation.squashautom.community-1.0.3.RELEASE.zip">.zip</a></td>
  </tr>
  <tr>
   <td>2.0.0, ou postérieure</td>
   <td>2.X.X</td>
   <td>2.0.2 : <a href="https://repo.squashtest.org/distribution/plugin.testautomation.squashautom.community-2.0.2.RELEASE.tar.gz">.tar.gz</a> ou <a href="https://repo.squashtest.org/distribution/plugin.testautomation.squashautom.community-2.0.2.RELEASE.zip">.zip</a></td>
  </tr>
  <tr>
   <td rowspan="3">Git Connector</td>
   <td>1.22.X, à partir de 1.22.2</td>
   <td>1.X.X</td>
   <td>1.0.3 : <a href="https://repo.squashtest.org/distribution/plugin.scm.git-1.0.3.RELEASE.tar.gz">.tar.gz</a> ou <a href="https://repo.squashtest.org/distribution/plugin.scm.git-1.0.3.RELEASE.zip">.zip</a></td>
  </tr>
  <tr>
   <td>2.0.X</td>
   <td>2.0.X</td>
   <td>2.0.0 : <a href="https://repo.squashtest.org/distribution/plugin.scm.git-2.0.0.RELEASE.tar.gz">.tar.gz</a> ou <a href="https://repo.squashtest.org/distribution/plugin.scm.git-2.0.0.RELEASE.zip">.zip</a></td>
  </tr>
  <tr>
   <td>2.1.0, ou postérieure</td>
   <td>2.X.X</td>
   <td>2.1.0 : <a href="https://repo.squashtest.org/distribution/plugin.scm.git-2.1.0.RELEASE.tar.gz">.tar.gz</a> ou <a href="https://repo.squashtest.org/distribution/plugin.scm.git-2.1.0.RELEASE.zip">.zip</a></td>
  </tr>
  <tr>
   <td rowspan="2">Actions Library</td>
   <td>1.22.X, à partir de 1.22.2</td>
   <td>1.X.X</td>
   <td rowspan ="2">pas de version Community</td>
  </tr>
  <tr>
   <td>2.0.0, ou postérieure</td>
   <td>2.X.X</td>
  </tr>
  <tr>
   <td rowspan="2">Workflow Automatisation Jira</td>
   <td>1.22.X, à partir de 1.22.2</td>
   <td>1.X.X</td>
   <td rowspan ="2">pas de version Community</td>
  </tr>
  <tr>
   <td>2.0.0, ou postérieure</td>
   <td>2.X.X</td>
  </tr>
  </tbody>
</table>

## Configuration de Squash TM

### Déclaration du serveur Squash Orchestrator

Afin de lancer un plan d'exécution manuellement depuis **Squash TM**, il est nécessaire de déclarer le serveur **Squash Orchestrator** qui exécutera les tests automatisés dans les environnements adaptés.  
Cette déclaration se fait dans la section *Serveurs d'exécution automatisée* de l'espace *Administration* :

=== "Squash TM 1.22.X"

    ![automation-server-declaration](resources/automation_servers_declaration.png){class="center"}

=== "Squash TM 2.X.Y"

    ![automation-server-declaration](resources/automation_servers_declaration_TM2.png){class="center"}

* ``Nom`` : Le nom du serveur tel qu'il apparaîtra dans l'espace *Cas de test*.

* ``Type`` : Sélectionnez *squashAutom* dans la liste déroulante.

* ``Url`` : L'adresse du service *Receptionist* de **Squash Orchestrator**.

!!! danger "Attention"
    Le service *bus d'évènements* de **Squash Orchestrator** doit impérativement être accessible à la même URL que le service *Receptionist* mais sur le port 38368.

Une fois le serveur créé, vous pouvez préciser un token servant pour l'authentification au serveur.

=== "Squash TM 1.22.X"

    ![token](resources/token.png){class="center"}

=== "Squash TM 2.X.Y"

    ![token](resources/token-TM2.png){class="center"}

!!! warning "Focus"
    La présence d'un token est obligatoire pour la bonne exécution de tests automatisés depuis **Squash TM**.  
    Dans le cas où l'authentification au serveur d'automatisation n'est pas requise par ce dernier, il faut quand même renseigner une valeur quelconque de token dans **Squash TM**

### Configuration du projet Squash TM

Une fois un serveur d'automatisation déclaré, il faut le lier à un projet Squash TM afin qu'il soit utilisé pour l'exécution des cas de test automatisés du projet.

La marche à suivre est la suivante :

=== "Squash TM 1.22.X"

    1. Allez dans l'espace ``[Administration]`` puis cliquez sur ``[Projets]``

    2. Sélectionnez un projet existant puis allez jusqu'à la section ``Automatisation``

        ![project-automation-block](resources/project-automation-block.png){class="fullpage"}

    3. Cliquez sur ``Pas de serveur``, une liste déroulante contenant les serveurs d'automatisation déclarés apparait

        ![project-automation-selection](resources/project-automation-selection.png){class="fullpage"}

    4. Sélectionnez le serveur et appuyer sur ``[Confirmer]``

=== "Squash TM 2.X.Y"

    1. Allez dans l'espace ``[Administration]`` puis cliquez sur ``[Projets]``

    2. Sélectionnez un projet existant puis allez jusqu'à la section ``Automatisation``

        ![project-automation-block](resources/project-automation-block-TM2.png){class="fullpage"}

    3. Cliquez sur ``Pas de serveur``, une liste déroulante contenant les serveurs d'automatisation déclarés apparait

        ![project-automation-selection](resources/project-automation-selection-TM2.png){class="fullpage"}

    4. Sélectionnez le serveur et confirmez la sélection

---
† indique un composant commun à **Squash AUTOM** et **Squash DEVOPS**. Il ne doit être installé qu'une seule fois pour les deux.
