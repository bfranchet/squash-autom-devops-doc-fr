# Automatisation avec JUnit

## Référence du test dans Squash TM

Pour lier un cas de test **Squash TM** à un test automatisé *JUnit*, le champ *Référence du test automatisé* du bloc *Automatisation* du cas de test doit avoir la forme suivante :

``[1] / [2] # [3]``

Avec :

* ``[1]`` : Nom du projet sur le dépôt de source.

* ``[2]`` : Nom qualifié de la classe de test.

* ``[3]`` : Nom de la méthode à tester dans la classe de test. Ce paramètre est optionnel.

Ci-dessous un exemple de classe de test *Junit* et l’automatisation du cas de test **Squash TM** associé :

=== "Squash TM 1.22.X"

    ![junit-1](resources/junit-test-1.png){class="fullpage"}

    ![junit-2](resources/junit-test-2.png){class="fullpage"}

=== "Squash TM 2.X.Y"

    ![junit-1](resources/junit-test-1.png){class="fullpage"}

    ![junit-2](resources/junit-test-2-TM2.png){class="fullpage"}

## Nature des paramètres Squash TM exploitables

Les paramètres **Squash TM** exploitables dans un script *JUnit* vont différer suivant si vous utilisez les composants **Squash DEVOPS Community** ou **Squash DEVOPS Premium**.

Voici le tableau des paramètres exploitables :

| Nature                                  | Clé            | Community               | Premium                 |
| :-------------------------------------: | :------------: | :---------------------: | :---------------------: |
| Nom du jeu de données                   | DSNAME         | ![OK](resources/ok.jpg) | ![OK](resources/ok.jpg) |
| Paramètre d'un jeu de données           | DS_[nom]       | ![OK](resources/ok.jpg) | ![OK](resources/ok.jpg) |
| Référence du cas de test                | TC_REF         | ![OK](resources/ok.jpg) | ![OK](resources/ok.jpg) |
| Champ personnalisé du cas de test       | TC_CUF_[code]  | ![OK](resources/ok.jpg) | ![OK](resources/ok.jpg) |
| Champ personnalisé de l'itération       | IT_CUF_[code]  | ![KO](resources/ko.jpg) | ![OK](resources/ok.jpg) |
| Champ personnalisé de la campagne       | CPG_CUF_[code] | ![KO](resources/ko.jpg) | ![OK](resources/ok.jpg) |
| Champ personnalisé de la suite de tests | TS_CUF_[code]  | ![KO](resources/ko.jpg) | ![OK](resources/ok.jpg) |

*Légende :*

* ``[code]`` : *valeur renseignée dans le champ “Code” d’un champ personnalisé*
* ``[nom]`` : *nom du paramètre tel que renseigné dans Squash TM*

## Utilisation de paramètres Squash TM

Il est possible lors de l’exécution d’un cas de test **Squash TM** automatisé avec *JUnit* d’exploiter des paramètres **Squash TM** au sein de celui-ci.

Pour cela, il faut suivre les étapes suivantes :

* Renseigner des champs personnalisés dans **Squash TM** et les associer au projet portant le plan de tests à exécuter.

* Importer la libraire *opentestfactory-java-param-library* dans le projet *JUnit* contenant les tests à exécuter en ajoutant au fichier ``pom.xml`` :

    * le dépôt Maven suivant :

    ``` mvn
    <repositories>
        <repository>
            <id>org.squashtest.repo.release</id>
            <name>Squashtest repository for releases</name>
            <url>https://repo.squashtest.org/maven2/release</url>
        </repository>
    </repositories>
    ```

    * la dépendance Maven suivante :

    ``` mvn
    <dependencies>
        <dependency>
            <groupId>org.opentestfactory.util</groupId>
            <artifactId>opentestfactory-java-param-library</artifactId>
            <version>1.0.0</version>
        </dependency>
    </dependencies>
    ```

* Vous pouvez ensuite récupérer la valeur d’un paramètre **Squash TM** du type souhaité en utilisant la syntaxe suivante :

``` java
ParameterService.INSTANCE.getString("paramName");
ParameterService.INSTANCE.getInt("paramName");
ParameterService.INSTANCE.getFloat("paramName");
ParameterService.INSTANCE.getDouble("paramName");
ParameterService.INSTANCE.getBoolean("paramName");
```

* Les méthodes ci-dessus recherchent le paramètre souhaité dans les *paramètres de tests* transmis par **Squash TM**; si elles ne le trouvent pas, elles le cherchent ensuite dans les *paramètres globaux* transmis par **Squash TM**.  
Ces méthodes propagent une ``ParameterNotFoundException`` si le paramètre n'est pas trouvé. Si le paramètre est trouvé mais ne peut être converti dans le type souhaité, une ``ParameterFormatException`` est propagée. Pensez à gérer ces exception dans vos classes de tests.  
Attention: la conversion de données booléennes renvoie ``true`` lorsque la chaine de caractère à convertir est égale à ``"true"`` (quelqu'en soit la casse), ``false`` dans tous les autres cas; mais ne propage jamais d'exception.

* Il est aussi possible de définir un paramètre par défaut dans le cas où le paramètre **Squash TM** n'est pas trouvé en utilisant la syntaxe suivante :

``` java
ParameterService.INSTANCE.getString("paramName", defaultValue);
ParameterService.INSTANCE.getInt("paramName", defaultValue);
ParameterService.INSTANCE.getFloat("paramName", defaultValue);
ParameterService.INSTANCE.getDouble("paramName", defaultValue);
ParameterService.INSTANCE.getBoolean("paramName", defaultValue);
```

* Les méthodes ci-dessus ne propagent donc pas de ``ParameterNotFoundException`` quand le paramètre **Squash TM** recherché n'est pas trouvé mais propagent une ``ParameterFormatException`` si le paramètre **Squash TM** est bien trouvé, mais ne peut être converti dans le type souhaité.
  
* Il est également possible de cibler un *paramètre de tests* ou un *paramètre global* avec des méthodes spécifiques. Comme pour les méthodes précedentes, elle sont disponibles dans des versions avec et sans paramètre par défaut. En voici quelques exemples :
  
``` java
ParameterService.INSTANCE.getTestString("paramName");
ParameterService.INSTANCE.getGlobalInt("paramName");
ParameterService.INSTANCE.getTestFloat("paramName", defaultValue);
ParameterService.INSTANCE.getGlobalBoolean("paramName", defaultValue);
```

Ci-dessous un exemple de fichier de test *JUnit* et l’automatisation du cas de test **Squash TM** associé :

=== "Squash TM 1.22.X"

    ![junit-params-1](resources/junit-test-with-param-1.png){class="center"}

    ![junit-params-2](resources/junit-test-with-param-2.png){class="center"}

    ![junit-params-3](resources/junit-test-with-param-3.png){class="center"}

=== "Squash TM 2.X.Y"

    ![junit-params-1](resources/junit-test-with-param-1.png){class="center"}

    ![junit-params-2](resources/junit-test-with-param-2-TM2.png){class="center"}

    ![junit-params-3](resources/junit-test-with-param-3-TM2.png){class="center"}


## Versions supportées

**Squash AUTOM** et **Squash DEVOPS** ont été validés avec

- JUnit 4.12, toute version plus récente de JUnit 4 devrait fonctionner
- JUnit 5.3.2, toute version de JUnit 5 devrait fonctionner

De plus, il est recommandé d'utiliser Surefire 2.19.1 ou supérieur.
