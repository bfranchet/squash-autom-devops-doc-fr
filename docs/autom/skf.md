# Automatisation avec SKF

## Référence du test dans Squash TM

Pour lier un cas de test **Squash TM** à un test automatisé, le champ *Référence du test automatisé* du bloc *Automatisation* du cas de test doit avoir une forme spécifique au framework de test utilisé :

``[1] / [2] . [3] # [4]``

Avec :

* ``[1]`` : Chemin du dossier *SKF* racine (qui contient le fichier pom.xml) sur le dépôt de source.

* ``[2]`` : Écosystème de tests par défaut du projet *SKF* (tests).

* ``[3]`` : Sous écosytème de tests (il est possible d'en rajouter plusieurs en les séparant par un ``.``; ce paramètre est optionnel).

* ``[4]`` : Nom du script de test à exécuter ( avec son extension ``.ta``; ce paramètre est obligatoire)

## Nature des paramètres Squash TM exploitables

Les paramètres **Squash TM** exploitables dans un script *SKF* vont différer suivant si vous utilisez les composants **Squash DEVOPS Community** ou **Squash DEVOPS Premium**.

Voici le tableau des paramètres exploitables :

| Nature                                  | Clé            | Community               | Premium                 |
| :-------------------------------------: | :------------: | :---------------------: | :---------------------: |
| Nom du jeu de données                   | DSNAME         | ![OK](resources/ok.jpg) | ![OK](resources/ok.jpg) |
| Paramètre d'un jeu de données           | DS_[nom]       | ![OK](resources/ok.jpg) | ![OK](resources/ok.jpg) |
| Référence du cas de test                | TC_REF         | ![OK](resources/ok.jpg) | ![OK](resources/ok.jpg) |
| Champ personnalisé du cas de test       | TC_CUF_[code]  | ![OK](resources/ok.jpg) | ![OK](resources/ok.jpg) |
| Champ personnalisé de l'itération       | IT_CUF_[code]  | ![KO](resources/ko.jpg) | ![OK](resources/ok.jpg) |
| Champ personnalisé de la campagne       | CPG_CUF_[code] | ![KO](resources/ko.jpg) | ![OK](resources/ok.jpg) |
| Champ personnalisé de la suite de tests | TS_CUF_[code]  | ![KO](resources/ko.jpg) | ![OK](resources/ok.jpg) |

*Légende :*

* ``[code]`` : *valeur renseignée dans le champ “Code” d’un champ personnalisé*
* ``[nom]`` : *nom du paramètre tel que renseigné dans Squash TM*

## Utilisation de paramètres Squash TM

Il est possible lors de l’exécution d’un cas de test **Squash TM** automatisé avec *SKF* d’exploiter des paramètres **Squash TM** au sein de celui-ci.

Pour cela, il faut suivre les étapes suivantes :

* Renseigner des champs personnalisés dans **Squash TM** et les associer au projet portant le plan de tests à exécuter.

* Faire appel aux paramètres désirés dans les fichiers utilisés par votre test SKF via la syntaxe `${clé}`

* Au sein du test SKF, appeler la commande pour remplacer au sein d'un fichier les références des paramètres par les valeurs transmises à l'exécution.  
  La commande à utiliser est de la forme `CONVERT  {resourceToConvert<Res:file>} TO file (param) USING context_global_params, context_script_params AS {converted<Res:file>}`

!!! info "Info"
    Retrouvez plus d'informations sur l'utilisation des paramètres dans un test SKF dans sa [documentation](https://skf.readthedocs.io/projects/skf/en/doc-stable/execution-reporting/execution-reporting-context-parameters.html#)

Ci-dessous un exemple de projet *SKF* exploitant un paramètre dans un fichier xml et l’automatisation du cas de test **Squash TM** associé :

=== "Squash TM 1.22.X"

    ![skf-params-1](resources/skf-test-with-params-1.png){class="center"}

    ![skf-params-3](resources/skf-test-with-params-3.png){class="center"}

    ![skf-params-2](resources/skf-test-with-params-2.png){class="center"}

=== "Squash TM 2.X.Y"

    ![skf-params-1](resources/skf-test-with-params-1.png){class="center"}

    ![skf-params-3](resources/skf-test-with-params-3.png){class="center"}

    ![skf-params-2](resources/skf-test-with-params-2-TM2.png){class="center"}

## Versions supportées

**Squash AUTOM** et **Squash DEVOPS** ont été validés avec SKF 1.14.0.
