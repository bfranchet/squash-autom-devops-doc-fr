# Squash AUTOM

Ce guide présente les possibilités offertes par **Squash AUTOM** pour exécuter des tests automatiques. La gestion de ces tests et du workflow de leur automatisation dans **Squash TM** est décrite dans [la documentation](https://tm-fr.doc.squashtest.com/user-guide/gestion-tests-automatises/concevoir-tests-automatises/) de ce dernier.

**Squash AUTOM** met à votre disposition les composants suivants (voir schéma ci-dessous) :

--8<--
shared/commonComponents.md
--8<--

* **Micro-services exclusifs à Squash AUTOM Premium**  
Posséder une licence **Squash AUTOM Premium** permet d'avoir accès à une image Docker contenant des micro-services pour **Squash Orchestrator** fournissant les fonctionnalités suivantes :
    * Gestion de l'exécution de tests Agilitest
    * Gestion de l'exécution de tests Ranorex
    * Gestion de l'exécution de tests UFT

* **Plugins pour Squash TM**

    * **Result Publisher**† : ce plugin permet la remontée d’informations vers **Squash TM** en fin d’exécution d’un plan d’exécution **Squash TM** par l’orchestrateur Squash.  
    Ce plugin existe en version **Community** (*squash.tm.rest.result.publisher.community*) librement téléchargeable ou **Premium** (*squash.tm.rest.result.publisher.premium*) accessible sur demande.

    * **Squash AUTOM** : ce plugin permet d'exécuter des tests automatisés depuis **Squash TM**.  
    Ce plugin existe en version **Community** (*plugin.testautomation.squashautom*) librement téléchargeable ou **Premium** (*plugin.testautomation.squashautom.premium*) accessible sur demande.

    * **Git Connector** : ce plugin permet de transmettre des cas de test scriptés rédigés dans **Squash TM** vers un gestionnaire de source Git.  
    Ce plugin **Community** (*plugin.scm.git*) est librement téléchargeable.

    * **Bibliothèque d'actions** : ce plugin permet de gérer, dans **Squash TM**, les actions des cas de test BDD via une bibliothèque.  
    Le plugin (*plugin.workspace.actionword*) n'est disponible qu'en **Premium** et est accessible sur demande.

    * **Workflow Automatisation Jira** : ce plugin permet d'externaliser (hors de **Squash TM**) le process d'automatisation des tests via un workflow d'automatisation personnalisé dans Jira.  
    Le plugin (*plugin.workflow.automjira*) n'est disponible qu'en **Premium** et est accessible sur demande.

Schéma de l'architecture haut niveau Squash AUTOM / Squash DEVOPS :
![schéma d'architecture](../resources/architecture.png){class=fullpage}
---
† indique un composant commun à **Squash AUTOM** et **Squash DEVOPS**.
