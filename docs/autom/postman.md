# Automatisation avec Postman

## Référence du test dans Squash TM

!!! info "Info"
    Le résultat de chaque exécution du cas de test **Squash TM** est calculé en prenant en compte les résultats individuels de chaque requête incluse dans le fichier collection lié, voire d'un niveau de répertoire spécifique de la collection :

    * Si au moins un test est en statut *Error* (dans le cas d'un problème technique), l'exécution sera en statut *Blocked*.
    * Si au moins un test a échoué fonctionnellement et qu'aucun test n'est en statut *Error*, l'exécution sera en statut *Failed*.
    * Si tous les tests ont réussi, l'exécution sera en statut *Success*.

Pour lier un cas de test **Squash TM** à un test automatisé *Postman*, le champ *Référence du test automatisé* du bloc *Automatisation* du cas de test doit avoir la forme suivante :

``[1] / [2] # [3] # [4]``

Avec :

* ``[1]`` : Nom du projet sur le dépôt de source.

* ``[2]`` : Chemin et nom du fichier de collection *Postman* à partir de la racine du projet (avec son extension ``.postman_collection.json``).

* ``[3]`` : Nom d'un répertoire spécifique dans la collection (option `--folder` de *Newman*). Ce paramètre est optionnel.

* ``[4]`` : Nom d'une requête spécifique à parser pour laquelle on désire obtenir un résultat en particulier.

!!! warning "Référence du test automatisé et exécution"
    La précision de la requête [4] dans la référence du test n'influe pas sur l'exécution, mais sur la détermination du résultat du test.  
    Ainsi, même en définissant une granularité à l'échelle de la requête, **la totalité des requêtes contenues dans le répertoire sera exécutée** (ce qui signifie, par exemple, que si plusieurs cas de test Squash TM pointent vers le même répertoire mais vers des requêtes différentes, toutes les requêtes du répertoire seront exécutées plusieurs fois).  
    Les rapports de test contiennent les traces de toutes les requêtes exécutées. Par contre, seuls les statuts *Error* / *Failed* / *Success* correspondant à cette granularité fine seront considérés pour définir le résultat du cas de test.

!!! warning "Unicité du répertoire spécifique"
    Si le répertoire racine du projet contient plusieurs répertoires ayant pour nom [3], *Newman* n'exécutera que le premier qu'il trouve. Il est donc fortement conseillé de choisir des noms uniques pour les répertoires de plus bas niveau.


## Nature des paramètres Squash TM exploitables

Les paramètres **Squash TM** exploitables dans un script *Postman* vont différer suivant si vous utilisez les composants **Squash DEVOPS Community** ou **Squash DEVOPS Premium**.

Voici le tableau des paramètres exploitables :

| Nature                                  | Clé            | Community               | Premium                 |
| :-------------------------------------: | :------------: | :---------------------: | :---------------------: |
| Nom du jeu de données                   | DSNAME         | ![OK](resources/ok.jpg) | ![OK](resources/ok.jpg) |
| Paramètre d'un jeu de données           | DS_[nom]       | ![OK](resources/ok.jpg) | ![OK](resources/ok.jpg) |
| Référence du cas de test                | TC_REF         | ![OK](resources/ok.jpg) | ![OK](resources/ok.jpg) |
| Champ personnalisé du cas de test       | TC_CUF_[code]  | ![OK](resources/ok.jpg) | ![OK](resources/ok.jpg) |
| Champ personnalisé de l'itération       | IT_CUF_[code]  | ![KO](resources/ko.jpg) | ![OK](resources/ok.jpg) |
| Champ personnalisé de la campagne       | CPG_CUF_[code] | ![KO](resources/ko.jpg) | ![OK](resources/ok.jpg) |
| Champ personnalisé de la suite de tests | TS_CUF_[code]  | ![KO](resources/ko.jpg) | ![OK](resources/ok.jpg) |

*Légende :*

* ``[code]`` : *valeur renseignée dans le champ “Code” d’un champ personnalisé*
* ``[nom]`` : *nom du paramètre tel que renseigné dans Squash TM*

## Utilisation de paramètres Squash TM

Il est possible lors de l’exécution d’un cas de test **Squash TM** automatisé avec *Postman* d’exploiter des paramètres **Squash TM** au sein de celui-ci.

Pour cela, il faut suivre les étapes suivantes :

* Renseigner des champs personnalisés dans **Squash TM** et les associer au projet portant le plan de tests à exécuter.

* S'assurer que les champs *code* des paramètres correspondent aux noms des variables d'environnement existants dans les requêtes *Postman*.

!!! info "Info"
    **Squash TM** ajoute un préfixe au *code* du champ personnalisé transmis. Assurez-vous de le prendre en compte.  
    Voir la [documentation](https://tm-fr.doc.squashtest.com/admin-guide/personnalisation-entites/gerer-champs-persos/) de **Squash TM** pour plus d'information.

Ci-dessous un exemple d'un fichier de collection *Postman* et l’automatisation du cas de test **Squash TM** associé :

=== "Squash TM 2.X.Y"

    ![postman-params-1](resources/postman-test-with-env-param-01.png){class="fullpage"}

    ![postman-params-2](resources/postman-test-with-env-param-02.png){class="fullpage"}

    ![postman-params-3](resources/postman-test-with-env-param-03.png){class="fullpage"}

    ![postman-params-4](resources/postman-test-with-env-param-03-TM2.png){class="fullpage"}

## Versions supportées

**Squash AUTOM** et **Squash DEVOPS** ont été validés avec Postman 8.12.1. Toute version récente devrait fonctionner.
