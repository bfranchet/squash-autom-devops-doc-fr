# FAQ

Cette section présente un ensemble de FAQ autour de **Squash AUTOM** et **Squash DEVOPS**.

Les FAQ disponibles sont les suivantes :

* [Généralités](./generalities.md) : FAQ sur des généralités autour de **Squash AUTOM**, **Squash DEVOPS** et les changements par rapport à **Squash TF**.
