# FAQ : Généralités autour de Squash AUTOM et Squash DEVOPS

Cette FAQ cherche à répondre à des questions sur le pourquoi de **Squash AUTOM** et **Squash DEVOPS**, ce que cela implique pour **Squash TF** et comment va s’opérer la transition de **Squash TF** à **Squash AUTOM** et **Squash DEVOPS**.

## Pourquoi deux nouveaux produits Squash AUTOM et Squash DEVOPS ?

La création de Squash AUTOM et Squash DEVOPS est le résultat d’une réflexion sur l’évolution des pratiques d’automatisation (essor des pratiques du CI/CD et du DevOps, utilisation de plus en plus démocratisée de la conteneurisation, multiplication des outils d’intégration) et sur comment la suite logicielle Squash pouvait être en phase avec celles-ci.

Il en est ressorti que Squash TF présentait des limites, notamment architecturales, pour son adoption au sein des principes DevOps.

C’est pourquoi nous avons décidé de développer un nouvel outil destiné à la gestion de l’exécution des tests automatisés respectant les principes suivants :

* Architecture micro-service, notamment pour des raisons de déploiement et d'exploitabilité en environnement DevOps.

* Séparation entre les fonctionnalités permettant d'automatiser (à destination des testeurs et automaticiens) et
  celles permettant d'intégrer les tests automatisés (pour le gestionnaire de pipeline) au sein de l'usine DevOps. Cela a donc donné naissance à 2 produits nommés Squash AUTOM et Squash DEVOPS.

* Suppression de l'adhérence avec Squash TM de manière à rendre ces deux produits indépendants de celui-ci.

## Quel est le modèle de Squash AUTOM et Squash DEVOPS ?

Le modèle retenu est un modèle « open core ».

Ce modèle, qui est le même que Squash TM, met à disposition deux versions :

* Une version Community gratuite composée d’un cœur open source et de modules freemium. Cette version est pleinement fonctionnelle (non bridée).

* Une version commerciale, avec souscription annuelle, composée de la version Community et de plugins commerciaux.  
  Elle apporte des fonctionnalités supplémentaires à valeur ajoutée, mais non indispensables, ainsi que le support.

## Quel est la fréquence des releases Squash AUTOM et Squash DEVOPS ?

Une nouvelle livraison de Squash AUTOM et Squash DEVOPS est disponible tous les deuxièmes mercredis de chaque mois (sauf août).
Elle contient la mise à jour de plus ou moins de composants selon les fonctionnalités ajoutées.
Elle est nommée par l'année et le mois (sous le format AAAA-MM).

## Squash AUTOM et Squash DEVOPS peuvent-ils s’utiliser sans Squash TM ?

Oui.

Notre but est que les deux produits apportent également de la valeur aux sociétés ou projets n’utilisant pas Squash TM :

* L'utilisation de Squash AUTOM "seul" permet ainsi d'unifier/d'homogénéiser l'usage des différents automates
  (Selenium, Cypress, SoapUI, Appium...) et des différents studios (Robot Framework, Cucumber, UFT, Agilitest...)
  tout en générant un format de reporting commun (type Allure).

* L'utilisation de Squash DEVOPS "seul" permet d'orchestrer l'ensemble des tests automatisés, de les intégrer
  au pipeline DevOps (CI/CD) puis de poster les résultats vers les destinataires (le pipeline lui-même,
  l'outil de patrimoine de test ou le framework de reporting et d'agrégation des résultats de test).

## Est-ce que de nouvelles fonctionnalités pour Squash TF arriveront dans le futur ?

Non, il n’y aura plus de nouvelles fonctionnalités développées pour Squash TF.

Nous vous encourageons à faire la transition de Squash TF à Squash AUTOM pour l’exécution de votre patrimoine de tests automatisés afin de profiter de l’ensemble des nouvelles fonctionnalités proposées par Squash.

Néanmoins, les éléments de Squash TF resteront accessibles en téléchargement. De même, les répertoires open source resteront accessibles.

## Le support pour Squash TF s’arrête-t-il avec la sortie de Squash AUTOM et Squash DEVOPS ?

Non.

Nous continuerons à assurer du support sur Squash TF via le forum Squashtest et, pour les clients de l’offre commerciale
Squash AUTOM, *via* notre service support.

## Mon patrimoine de tests automatisés, exécutés jusque-là avec Squash TF, doit-il être modifié pour être utilisé avec Squash AUTOM ?

Non.

Les scripts/tests automatisés que vous exécutiez *via* Squash TF sont exploitables par Squash AUTOM sans modification de ceux-ci.

## Puis-je exécuter des tests SKF avec Squash AUTOM et Squash DEVOPS ?

Le support des tests SKF est disponible à partir de la version 2021-07.

## Que dois-je faire dans Squash TM pour lancer mes plans d’exécutions automatisées avec Squash AUTOM au lieu de Squash TF ?

Il est nécessaire de créer un lien entre un cas de test Squash TM et votre test automatisé conformément à la documentation de Squash AUTOM.

Cette action est quasi instantanée et peut se faire en masse pour vos cas de test Squash TM Gherkin ou BDD exploitant le plugin Git.  
Pour les autres cas de test, une action sur chaque cas de test sera nécessaire conformément à la documentation de Squash AUTOM.

L’action de lien entre un cas de test Squash TM et un test automatisé pour une exécution avec Squash AUTOM est différente de celle pour une exécution avec Squash TF.

## Puis-je mélanger dans un même plan d’exécution des cas de tests automatisés exécutés par Squash TF et des cas de tests exécutés par Squash AUTOM ?

Oui.

Afin de faciliter la transition, il est parfaitement possible d’avoir, au sein d’un même plan d’exécution Squash TM,
des cas de tests issus d’un projet exploitant Squash TF et des cas de tests issus d’un projet exploitant Squash AUTOM.

## Dois-je forcément avoir un serveur Jenkins pour pouvoir exécuter mes tests automatisés depuis Squash TM via Squash AUTOM ?

Non.

Les jobs Jenkins spécifiques nécessaires pour l’exécution de tests automatisés depuis Squash TM via Squash TF ne sont plus
un prérequis pour une exécution depuis Squash TM via Squash AUTOM.

Avec Squash AUTOM, l’exécution est assurée par le Squash Orchestrator, un composant spécifique de Squash AUTOM.

## Puis-je lancer mes plans d’exécutions automatisées Squash TM depuis un pipeline Jenkins avec Squash AUTOM et Squash DEVOPS ?

Oui.

L’exécution d’un plan d’exécution Squash TM depuis un pipeline Jenkins est une nouveauté de Squash DEVOPS par rapport à Squash TF et nécessite la mise en place de jobs suivant les indications de la documentation Squash DEVOPS.
