# Mentions légales

Les mentions légales applicables sont celles de la version la plus récente du site : [https://autom-devops-fr.doc.squashtest.com/latest/mentions-legales.html](https://autom-devops-fr.doc.squashtest.com/latest/mentions-legales.html).

## Editeur du site

Le site **https://autom-devops-fr.doc.squashtest.com/** est la propriété de HENIX, SAS au capital de 81 527€, ayant son siège social au 1 rue François Ory, 92120 MONTROUGE, immatriculée sous le numéro 421 479 163 RCS PARIS.

TVA : FR15421479163

Téléphone : 01 42 31 02 00

Mail : contact@henix.com

​Directeur de la publication : Laurent Mazuré

## Hébergeur

GitLab

## Propriété intellectuelle et industrielle

Ce site internet est la propriété d’Henix.

Les marques figurant sur le présent site sont des marques déposées et protégées et sont la propriété exclusive d'Henix.

Toute extraction, par transfert permanent ou temporaire, de la totalité ou d'une partie du contenu du présent site internet sur un autre support, par tout moyen ou sous toute forme que ce soit, ainsi que la réutilisation, par la mise à disposition du public de la totalité ou d'une partie du contenu du présent site internet, quelle qu'en soit la forme, est illicite. Tous les éléments (textes, commentaires, ouvrages, illustrations, logos, marques, vidéos et images, sans que cette liste soit limitative) affichés ou cités sur le présent site internet sont la propriété exclusive de leurs titulaires respectifs. Conformément au Code de la Propriété Intellectuelle, toute utilisation ou reproduction totale ou partielle desdits éléments effectuée à partir du présent site internet sans l'autorisation expresse et écrite d’Henix est interdite.

## Crédits icônes incluses sur ce site

[Icones8](https://icones8.fr/)

## Utilisation du site

Henix met en œuvre tous les moyens à sa disposition pour assurer la fiabilité de l’utilisation du site et de ses contenus mais ne donne aucune garantie et ne saurait être responsable de l’utilisation faite du présent site internet. Notamment, Henix ne saurait être tenu responsable en cas d’erreurs, d’interruptions, de dysfonctionnements, de perte de données ou de tout autre événement qui impacterait l’utilisation du site internet. De même, Henix ne saurait voir sa responsabilité engagée en raison de la nature ou du contenu des pages constituant le présent site internet ou des sites tiers référencés sur les pages du présent site internet notamment ceux pour lesquels il existe un lien hypertexte.