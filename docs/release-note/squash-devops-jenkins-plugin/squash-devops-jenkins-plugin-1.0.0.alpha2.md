# Squash DEVOPS plugin 1.0.0.alpha2 Release Note

## Introduction

Squash DEVOPS plugin is a plugin for Jenkins to register Squash Orchestrator instance in Jenkins configuration and send execution plan to a Squash Orchestrator instance from a pipeline.

This release note describes the changes of Test Plan Retriever plugin version 1.0.0.alpha2 compared to version 1.0.0.alpha1.

Squash DEVOPS plugin 1.0.0.alpha2 is compatible with version 2.164.1 or higher of Jenkins.

Squash DEVOPS plugin 1.0.0.alpha2 was released on March 16, 2021.

----

## Bug fixes

* [SQUASH-2976] Renaming pipeline method runSquashTFWorkflow() to runSquashTFWorkflow()
