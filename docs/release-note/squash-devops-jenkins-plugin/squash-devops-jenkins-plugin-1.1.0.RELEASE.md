# Squash DEVOPS plugin 1.1.0.RELEASE Release Note

## Introduction

Squash DEVOPS plugin is a plugin for Jenkins to register Squash Orchestrator instance in Jenkins configuration and send execution plan to a Squash Orchestrator instance from a pipeline.

This release note describes the changes of Squash DEVOPS plugin version 1.1.0.RELEASE compared to version 1.0.0.RELEASE.

Squash DEVOPS plugin 1.1.0.RELEASE is compatible with version 2.164.1 or higher of Jenkins.

Squash DEVOPS plugin 1.1.0.RELEASE was released on June 04, 2021.

----

## Major features of the release

* [SQUASH-3558] First level step from PEAC are logged in jenkins pipeline

## Bug fixes

* [SQUASH-3729] Runs-on attribute in a PEAC can be a single String instead of an array.
* [SQUASH-3751] A PEAC with doublon in job names is now rejected.
