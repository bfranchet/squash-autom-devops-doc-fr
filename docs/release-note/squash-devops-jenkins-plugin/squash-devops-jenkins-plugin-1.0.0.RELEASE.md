# Squash DEVOPS plugin 1.0.0.RELEASE Release Note

## Introduction

Squash DEVOPS plugin is a plugin for Jenkins to register Squash Orchestrator instance in Jenkins configuration and send execution plan to a Squash Orchestrator instance from a pipeline.

This release note describes the changes of Test Plan Retriever plugin version 1.0.0.RELEASE compared to version 1.0.0.alpha2.

Squash DEVOPS plugin 1.0.0.RELEASE is compatible with version 2.164.1 or higher of Jenkins.

Squash DEVOPS plugin 1.0.0.RELEASE was released on April 23, 2021.

----

## Bug fixes

* [SQUASH-3093] Renaming the artefact.
