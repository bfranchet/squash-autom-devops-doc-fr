# Squash DEVOPS Jenkins plugin 1.2.0.RELEASE Release Note

## Introduction

Squash DEVOPS Jenkins plugin is a plugin for Jenkins to register Squash Orchestrator instance in Jenkins configuration and send execution plan to a Squash Orchestrator instance from a pipeline.

This release note describes the changes of Squash DEVOPS Jenkins plugin version 1.2.0.RELEASE compared to version 1.1.0.RELEASE.

Squash DEVOPS Jenkins plugin 1.2.0.RELEASE is compatible with version 2.164.1 or higher of Jenkins.

Squash DEVOPS Jenkins plugin 1.2.0.RELEASE was released on July 15, 2021.

----

## Major features of the release

* [SQMAP-623] Squash DEVOPS Jenkins Plugin can execute PEaC using Inception execution environment.
