# Squash DEVOPS Jenkins plugin 1.3.0 Release Note

## Introduction

Squash DEVOPS Jenkins plugin is a plugin for Jenkins to register Squash Orchestrator instance in Jenkins configuration and send execution plan to a Squash Orchestrator instance from a pipeline.

This release note describes the changes of Squash DEVOPS Jenkins plugin version 1.3.0 compared to version 1.2.0.

Squash DEVOPS Jenkins plugin 1.3.0 is compatible with version 2.164.1 or higher of Jenkins.

Squash DEVOPS Jenkins plugin 1.3.0 was released on September 9, 2021.

----

## New features

* The plugin has been updated to support PEaCs containing hooks (see <a href="https://opentestfactory.gitlab.io/orchestrator/guides/hooks/" target="_blank">OTF orchestrator documentation</a>).
