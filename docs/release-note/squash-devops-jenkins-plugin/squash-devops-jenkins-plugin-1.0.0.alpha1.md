# Squash DEVOPS plugin 1.0.0.alpha1 Release Note

## Introduction

Squash DEVOPS plugin is a plugin for Jenkins to register Squash Orchestrator instance in Jenkins configuration and send execution plan to a Squash Orchestrator instance from a pipeline.
This release note explains new features of Test Plan Retriever plugin version 1.0.0.alpha1.

Squash DEVOPS plugin 1.0.0.alpha1 was released on February 05, 2021.

## Major features of the release

* New section in Jenkins configuration page to register Squash Orchestrator instances
* New pipeline method to send an execution plan with Json or YAML format to a Squash Orchestrator instance
