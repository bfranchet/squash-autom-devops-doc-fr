# Squash Orchestrator 1.1.0.RELEASE Release Note

## Introduction

Squash Orchestrator is a set of micro-services to perform automated test execution workflows described in a specific format.

This release note describes the changes of Squash Orchestrator version 1.1.0.RELEASE compared to 1.0.0.RELEASE.

Squash Orchestrator 1.1.0.RELEASE was released on June 04, 2021.

----

## Evolutions in existing micro-services

* The Docker image support a PUBLIC_KEY environment variable at launch to set up a single trusted key
* Improvement of OTF Orchestrator Documentation

### **Squash TM Generator service**

* Generic:
    * [SQUASH-3645] Generator service generates user friendly names for generated jobs

### **Robot Framework action provider**

* Adaptation to work with Windows execution environment

----

## Bug fixes

* Fix race window on job attribution
* Handling of job with no step
* [SQUASH-3545] Fix Robot Framework report parsing when extracting status of a full .robot file
* [SQUASH-3731] Fix JUnit report parsing when extracting status of a full class file
