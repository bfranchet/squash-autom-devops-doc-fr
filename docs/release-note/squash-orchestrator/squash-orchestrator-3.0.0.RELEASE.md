# Squash Orchestrator 3.0.0 Release Note

## Introduction

Squash Orchestrator is a set of micro-services to perform automated test execution workflows described in a specific format.

This release note describes the changes of Squash Orchestrator version 3.0.0 compared to 2.2.0.

Squash Orchestrator 3.0.0 was released on November 16, 2021.

----

## New features

* Support of Postman tests: <a href="https://www.postman.com/" target="_blank">Postman</a> allows writing test scripts for validating REST, SOAP, GraphQL… Web Services. It is an alternative to SoapUI which was already supported. It is now possible to write PEaC files executing some Postman tests as documented in <a href="https://opentestfactory.gitlab.io/orchestrator/providers/postman/" target="_blank">https://opentestfactory.gitlab.io/orchestrator/providers/postman/</a>.

* An <a href="https://docs.qameta.io/allure/" target="_blank">Allure</a> report is now produced for each executed PEaC. If the orchestrator is configured to publish the reports in a S3 bucket, the Allure report will be pushed in that bucket.  
(Due to some technical constraints, Ranorex results are currently not taken into account in the Allure report. This should be corrected in a near future release.)
