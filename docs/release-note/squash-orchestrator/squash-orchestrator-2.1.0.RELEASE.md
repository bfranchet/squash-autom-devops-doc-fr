# Squash Orchestrator 2.1.0 Release Note

## Introduction

Squash Orchestrator is a set of micro-services to perform automated test execution workflows described in a specific format.

This release note describes the changes of Squash Orchestrator version 2.1.0 compared to 2.0.0.

Squash Orchestrator 2.1.0 was released on September 9, 2021.

----

## New features

* Support of hooks (see <a href="https://opentestfactory.gitlab.io/orchestrator/guides/hooks/" target="_blank">OTF orchestrator documentation</a>).

----

## Bug fixes

* SQUASH-4037 [ActionProvider] Contrôle des versions<br>
When executing a PEaC, the versions of the actions are now properly checked (previously, they were ignored).

* SQUASH-4227 Unavailable hosts not fully ignored in SSH channel<br>
The orchestrator does not push anymore jobs to unavailable hosts.

* SQUASH-4289 Sur l'orchestrateur de production, l'exécution de certains TI junit produit un blocage<br>
JUnit tests producing a lot of output are not stuck anymore.

* SQUASH-4140 SKF - Inception : Les rapports XML ne sont plus récupérés<br>
The support of SKF in inception mode has been fixed.

* SQUASH-4149 Action params : Les accents dans les CUF cassent l'exécution<br>
All characters can now be used in Squash TM's CUFs.
