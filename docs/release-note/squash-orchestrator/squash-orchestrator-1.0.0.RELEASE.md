# Squash Orchestrator 1.0.0.RELEASE Release Note

## Introduction

Squash Orchestrator is a set of micro-services to perform automated test execution workflows described in a specific format.

This release note describes the changes of Squash Orchestrator version 1.0.0.RELEASE compared to 1.0.0.alpha2.

Squash Orchestrator 1.0.0.RELEASE was released on April 23, 2021.

----

## Major features of the release

* Exploit following actions in yours PEaC:
    * Launch Agilitest tests
    * Exploitation of Robot Framework results and Agilitest results in generated Allure report
* New Docker Image for Squash AUTOM Premium only micro-services. Following micro-services are added to this image:
    * Agilitest action provider service
    * Agilitest report interpretor service
* OpenTestFactory agent for communication over HTTP between a Squash Orchestrator and a test execution environment.

----

## New micro-services introduced by the release

### **Agilitest report interpretor service**

Service to extract execution results from a Agilitest type xml report.

### **Agilitest action provider service**

Service to allow Agilitest test related actions inside a PEaC.

Following actions are supported:

* Execute an Agilitest test
* Generate parameters to be used during test execution

----

## Evolutions in existing micro-services

### **Squash TM Generator service**

* Generic:
    * [SQUASH-1924] Generator service specifies environment execution depending on automated test technology information received from Squash TM

### **Robot Framework action provider service**

* [SQUASH-3040] Add option to generate (or not) an Allure report with Robot Framework tests results

### **Cucumber report interpretor service**

* [SQUASH-3056] Improve test reference possibilities for test report parsing

### **Allure report collector service**

* [SQUASH-3040] Robot Framework test results can be used when generating Allure report

----

## Bug fixes

* [SQUASH-3080] Fix false positive result sent to Squash TM when an automated test reference does not exist for Cucumber, Junit, SoapUI or Cypress tests.
* [SQUASH-3091] Fix to avoid NPE situation in Squash TM publisher micro-service when sending Allure report
* [SQUASH-3102] Fix thread synchronization issues resulting in untransmitted results to Squash TM
* [SQUASH-3092] Fix duplicate test case results in Allure Report in "dry/run/inception" situation
* [SQUASH-3201] Fix concurrent access issue with Squash TM result publisher plugin
* [SQUASH-1928] Fix NPE situation if tagValue or tagLabel are empty in a Squash TM Generator step call of a PEaC with YAML format
* [SQUASH-3068] Fix wrong result sending to Squash TM for Cypress test execute in dryrun/inception environment
* [SQUASH-3070] Fix wrong result sending to Squash TM for Robot Framework test execute in dryrun/inception environment when more than one ITPI are linked to a Robot Framework test case with same name
* [SQUASH-3399] Fix Robot Framework report parsing based on several .robot file execution in dry-run/inception situation
