# Squash Orchestrator 1.0.0.alpha2 Release Note

## Introduction

Squash Orchestrator is a set of micro-services to perform automated test execution workflows described in a specific format.

This release note describes the changes of Squash Orchestrator version 1.0.0.alpha1 compared to 1.0.0.alpha2.

Squash Orchestrator 1.0.0.alpha2 was released on March 16, 2021.

----

## Major features of the release

* Create Planned Execution "as Code" (PEaC) in a Json or YAML to orchestrate executions of your automated tests outside a test referential.
* Exploit following actions in yours PEaC:
    * Manage branch when cloning a Git repository
    * Launch Cucumber tests
    * Launch SoapUI tests
    * Launch Cypress tests
    * Generate an Allure report based on all execution reports from a PEaC workflow execution
    * Send Allure report to a Squash TM instance to be attached to an Automated Suite
* Squash TM Generator "Dry-run" mode: Squash Orchestrator doesn't execute tests of a Squash TM execution plan but generate execution results based on a list of execution reports given by the user.

----

## New micro-services introduced by the release

### **Cucumber report interpretor service**

Service to extract execution results from a Cucumber type xml report.

### **Cypress report interpretor service**

Service to extract execution results from a Cypress type xml report.

### **SoapUI report interpretor service**

Service to extract execution results from a SoapUI type xml report.

### **Allure Report Collector**

Service to generate an Allure report based on execution reports created during a PEaC workflow execution.

### **Cucumber action provider service**

Service to allow Cucumber test related actions inside a PEaC.

Following actions are supported:

* Execute a Cucumber test
* Specify a tag to execute

### **Cypress action provider service**

Service to allow Cypress test related actions inside a PEaC.

Following actions are supported:

* Execute a Cypress test
* Generate environment parameters

### **SoapUI action provider service**

Service to allow SoapUI test related actions inside a PEaC.

Following actions are supported:

* Execute a SoapUI test

### **Inception service**

Service to execute PEaC isntructions on a "fake" execution environment in order to send execution result message with the attachments given as arguments by the user.

----

## Evolutions in existing micro-services

### **Squash TM Generator service**

* Generic:
    * Send Allure report to Squash TM at the end of a Squash TM execution plan execution
    * Support of "Dry-run" mode
    * Can generate workflow messages for SoapUI Test execution
    * Can generate workflow messages for Cucumber Test execution
    * Can generate workflow messages for Cypress Test execution
    * Can generate workflow messages for Git clone action with branch and authentification information received from Squash TM

### **Git action provider service**

* Can specify the name of the branch to clone
* Can retrieve authentication informations from the execution environment for connection to the scm server
