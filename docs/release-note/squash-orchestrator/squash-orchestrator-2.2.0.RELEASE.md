# Squash Orchestrator 2.2.0 Release Note

## Introduction

Squash Orchestrator is a set of micro-services to perform automated test execution workflows described in a specific format.

This release note describes the changes of Squash Orchestrator version 2.2.0 compared to 2.1.0.

Squash Orchestrator 2.2.0 was released on October 21, 2021.

----

## New features

* Support for properly starting and stopping the orchestrator. In order to simplify these operations, some [tools](../orchestrator-tools/orchestrator-tools-0.20.1.md) are provided: the start script will return when the orchestrator is fully up and running; the stop script will ensure that all current tasks are finished before shutting down the orchestrator.  
This can be used to start the orchestrator, execute some tests, and stop the orchestrator in a step of a CI/CD pipeline.    
This is also useful for the upgrading an orchestrator used "as a service" (i.e. continuously up-and-running).

* Addition of the *put-file* action to put a file in the execution environment (see <a href="https://opentestfactory.gitlab.io/orchestrator/providers/actionprovider/#actionsput-filev1" target="_blank">the action description</a> in the OTF documentation).

* Some technical refactoring.

----

## Bug fixes

* SQUASH-4340 OTF core max_step (256) is too low : we can't FIT a 20-30 test case test plan in a job<br>
The orchestrator is able to support test plans with many more tests.
