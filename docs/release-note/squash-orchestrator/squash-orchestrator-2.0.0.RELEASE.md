# Squash Orchestrator 2.0.0 Release Note

## Introduction

Squash Orchestrator is a set of micro-services to perform automated test execution workflows described in a specific format.

This release note describes the new features of Squash Orchestrator version 2.0.0, compared to 1.1.0.

Squash Orchestrator 2.0.0 was released on July 15, 2021.

----

## Major features of the release

* Exploit following actions in yours PEaC:
    * Launch SKF test on Linux or Windows environment
* Exploitation of SKF results in generated Allure report
* Compatibility of Cucumber, Cypress and JUnit action provider services with executions on Windows environments

----

## OpenTestFactory related services

### New micro-services introduced by the release

#### **SKF action provider service**

Service to allow SKF test related actions inside a PEaC.

Following actions are supported:

* Execute a SKF test
* Generate parameters to be used during SKF test execution

### Evolutions in existing micro-services

* Improvement of OTF documentation

#### OTF/Python toolkit

* Several methods added for quicker and easier OTF service development
* Add hook methods to alter default behavior of an action provider action

#### Cucumber action provider service

* [SQUASH-3801] Compatibility with execution on Windows environment

#### Cypress action provider service

* [SQUASH-3802] Compatibility with execution on Windows environment

#### Files action provider service

* Add get-file action

#### JUnit action provider service

* Refactoring to use get-file action instead of custom command line
* [SQUASH-3800] Compatibility with execution on Windows environment

#### Robot Framework action provider service

* Refactoring to use get-file action instead of custom command line

#### Surefire report interpreter service

* [SQUASH-3796] Support for SKF report parsing

### Bug fixes

#### Robot Framework report interpreter service

* [SQUASH-3797] Wrong report parsing if tests have been executed on windows environment

----

## Squash Orchestrator specific services

### Evolutions in existing micro-services

#### Squash TM Generator service

* Generic:
    * [SQUASH-3659] Refactoring of the service to use a library shared with other modules of Squashtest.
