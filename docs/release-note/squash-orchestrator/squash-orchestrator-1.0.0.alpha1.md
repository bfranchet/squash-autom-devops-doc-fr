# Squash Orchestrator 1.0.0.alpha1 Release Note

## Introduction

Squash Orchestrator is a set of micro-services to perform automated test execution workflows described in a specific format.

This release note explains new features of Squash Orchestrator version 1.0.0.alpha1.

Squash Orchestrator 1.0.0.alpha1 was released on February 05, 2021.

## Major features of the release

* All micro-services available in a unique Docker image.
* Create Planned Execution "as Code" (PEaC) in a Json or YAML to orchestrate executions of your automated tests outside a test referential.
* Exploit following actions in yours PEaC:
    * Clone automated tests from a distant Git repository
    * Launch Robot Framework tests
    * Launch JUnit tests
    * Upload execution reports to an Amazon S3 container
    * Launch specific Squash TM automated execution plans and send back execution result and execution reports at the end of execution

## Micro-services introduced by the release

### **EventBus service**

Service to allow publish/subscribe type commmunication between Squash Orchestrator micro-services

### **Receptionist service**

Service used to receive PEaC and launch its execution.

### **Arranger service**

Service used to manage workflows of PEaC execution

### **Observer service**

Service used to retrieve information about a PEaC workflow.

### **Killswitch service**

Service to cancel the execution of a PEaC workflow

### **Squash TM Generator service**

Service to retrieve a Squash TM automated execution plan and execute it.

* Community version:
    * Can retrieve execution plan of a Squash TM Iteration or Test Suite
    * Retrieve an execution plan with following informations about test cases:
        * Test case dataset
        * Test case custom fields

* Premium version:
    * Can retrieve execution plan of a Squash TM Iteration or Test Suite
    * Can filter execution plan on a test case's tag custom field value
    * Retrieve an execution plan with following informations about test cases:
        * Test case dataset
        * Test case custom fields

### **Squash TM publisher service**

Service to send execution results and execution reports to a Squash TM instance.

### **Surefire report interpretor service**

Service to extract execution results from a Surefire type xml report.

### **Robot Framework report interpretor service**

Service to extract execution results from a Robot Framework type xml report.

### **Git action provider service**

Service to allow git repository cloning action inside a PEaC

### **Files action provider service**

Service to allow file manipulation action inside a PEaC.

Following actions are supported:

* Create a file
* Retrieve a file

### **JUnit action provider service**

Service to allow JUnit test related actions inside a PEaC.

Following actions are supported:

* Execute a JUnit test

### **Robot Framework action provider service**

Service to allow Robot Framework test related actions inside a PEaC.

Following actions are supported:

* Execute a Robot Framework test
* Create a parameter file

### **S3 publisher service**

Service to publish execution reports to an Amazon S3 container.

### **SSHEE service**

Service to send PEaC instructions to an execution environment by SSH protocol
