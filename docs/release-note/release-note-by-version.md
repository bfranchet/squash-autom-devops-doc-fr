# Release notes par version des composants de Squash AUTOM et Squash DEVOPS

Retrouvez ci-dessous les release notes pour les composants de Squash AUTOM et Squash DEVOPS, organisées par version.

## 2021-11

* [Squash Orchestrator 3.0.0](squash-orchestrator/squash-orchestrator-3.0.0.RELEASE.md)
* [Outils de l'orchestrateur 0.20.2](orchestrator-tools/orchestrator-tools-0.20.2.md)

## 2021-10

* [Squash Orchestrator 2.2.0](squash-orchestrator/squash-orchestrator-2.2.0.RELEASE.md)
* [Agent 1.1.2](agent/agent-1.1.2.RELEASE.md)
* [Outils de l'orchestrateur 0.20.1](orchestrator-tools/orchestrator-tools-0.20.1.md)
* [Micro-services exclusifs à Squash AUTOM Premium 3.0.2](squash-orchestrator-premium-services/squash-orchestrator-premium-services-3.0.2.RELEASE.md)
* [Plugin Squash AUTOM pour Squash TM 2.0.2](squash-autom-plugin/squash-autom-plugin-2.0.2.RELEASE.md)
* [Plugin Squash AUTOM pour Squash TM 1.0.3](squash-autom-plugin/squash-autom-plugin-1.0.3.RELEASE.md)

## 2021-09

* [Squash Orchestrator 2.1.0](squash-orchestrator/squash-orchestrator-2.1.0.RELEASE.md)
* [Micro-services exclusifs à Squash AUTOM Premium 3.0.1](squash-orchestrator-premium-services/squash-orchestrator-premium-services-3.0.1.RELEASE.md)
* [Plugin Result Publisher pour Squash TM 2.1.0](result-publisher-plugin/result-publisher-plugin-2.1.0.RELEASE.md)
* [Plugin Squash AUTOM pour Squash TM 2.0.1](squash-autom-plugin/squash-autom-plugin-2.0.1.RELEASE.md)
* [Plugin Squash AUTOM pour Squash TM 1.0.2](squash-autom-plugin/squash-autom-plugin-1.0.2.RELEASE.md)
* [Plugin Squash DEVOPS pour Jenkins 1.3.0](squash-devops-jenkins-plugin/squash-devops-jenkins-plugin-1.3.0.RELEASE.md)
* [Java Param Library 1.0.0](java-param-library/java-param-library-1.0.0.RELEASE.md)

## 2021-07

* [Squash Orchestrator 2.0.0](squash-orchestrator/squash-orchestrator-2.0.0.RELEASE.md)
* [Micro-services exclusifs à Squash AUTOM Premium 3.0.0](squash-orchestrator-premium-services/squash-orchestrator-premium-services-3.0.0.RELEASE.md)
* [Plugin Result Publisher pour Squash TM 2.0.0](result-publisher-plugin/result-publisher-plugin-2.0.0.RELEASE.md)
* [Plugin Test Plan Retriever pour Squash TM 2.0.0](test-plan-retriever-plugin/test-plan-retriever-plugin-2.0.0.RELEASE.md)
* [Plugin Squash AUTOM pour Squash TM 2.0.0](squash-autom-plugin/squash-autom-plugin-2.0.0.RELEASE.md)
* [Plugin Squash AUTOM pour Squash TM 1.0.1](squash-autom-plugin/squash-autom-plugin-1.0.1.RELEASE.md)
* [Plugin Squash DEVOPS pour Jenkins 1.2.0](squash-devops-jenkins-plugin/squash-devops-jenkins-plugin-1.2.0.RELEASE.md)

## 1.1.0 (juin 2021)

* [Squash Orchestrator 1.1.0](squash-orchestrator/squash-orchestrator-1.1.0.RELEASE.md)
* [Micro-services exclusifs à Squash AUTOM Premium 2.0.0](squash-orchestrator-premium-services/squash-orchestrator-premium-services-2.0.0.RELEASE.md)
* [Plugin Squash DEVOPS pour Jenkins 1.1.0](squash-devops-jenkins-plugin/squash-devops-jenkins-plugin-1.1.0.RELEASE.md)

## 1.0.0 (avril 2021)

* [Squash Orchestrator 1.0.0](squash-orchestrator/squash-orchestrator-1.0.0.RELEASE.md)
* [Micro-services exclusifs à Squash AUTOM Premium 1.0.0](squash-orchestrator-premium-services/squash-orchestrator-premium-services-1.0.0.RELEASE.md)
* [Plugin Result Publisher pour Squash TM 1.0.0](result-publisher-plugin/result-publisher-plugin-1.0.0.RELEASE.md)
* [Plugin Test Plan Retriever pour Squash TM 1.0.0](test-plan-retriever-plugin/test-plan-retriever-plugin-1.0.0.RELEASE.md)
* [Plugin Squash DEVOPS pour Jenkins 1.0.0](squash-devops-jenkins-plugin/squash-devops-jenkins-plugin-1.0.0.RELEASE.md)
* [Plugin Squash AUTOM pour Squash TM 1.0.0](squash-autom-plugin/squash-autom-plugin-1.0.0.RELEASE.md)

## 1.0.0.alpha2 (mars 2021)

* [Squash Orchestrator 1.0.0.alpha2](squash-orchestrator/squash-orchestrator-1.0.0.alpha2.md)
* [Plugin Result Publisher pour Squash TM 1.0.0.alpha2](result-publisher-plugin/result-publisher-plugin-1.0.0.alpha2.md)
* [Plugin Test Plan Retriever pour Squash TM 1.0.0.alpha2](test-plan-retriever-plugin/test-plan-retriever-plugin-1.0.0.alpha2.md)
* [Plugin Squash DEVOPS pour Jenkins 1.0.0.alpha2](squash-devops-jenkins-plugin/squash-devops-jenkins-plugin-1.0.0.alpha2.md)
* [Plugin Squash AUTOM pour Squash TM 1.0.0.alpha2](squash-autom-plugin/squash-autom-plugin-1.0.0.alpha2.md)

## 1.0.0.alpha1 (février 2021)

* [Squash Orchestrator 1.0.0.alpha1](squash-orchestrator/squash-orchestrator-1.0.0.alpha1.md)
* [Plugin Result Publisher pour Squash TM 1.0.0.alpha1](result-publisher-plugin/result-publisher-plugin-1.0.0.alpha1.md)
* [Plugin Test Plan Retriever pour Squash TM 1.0.0.alpha1](test-plan-retriever-plugin/test-plan-retriever-plugin-1.0.0.alpha1.md)
* [Plugin Squash DEVOPS pour Jenkins 1.0.0.alpha1](squash-devops-jenkins-plugin/squash-devops-jenkins-plugin-1.0.0.alpha1.md)
