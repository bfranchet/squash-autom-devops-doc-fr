# Squash Autom plugin 1.0.0.RELEASE Release Note

## Introduction

Squash AUTOM plugin is a plugin for Squash TM to allow automated items execution from Squash TM by Squash AUTOM.

This release note describes the changes of Squash AUTOM plugin version 1.0.0.RELEASE compared to version 1.0.0.alpha2.

Squash AUTOM plugin 1.0.0.RELEASE is compatible with Squash TM version 1.22.2.RELEASE or higher.

Squash AUTOM plugin 1.0.0.RELEASE was released on April 23, 2021.

The plugin exists in two versions: Community (free) and Premium (commercial).

----

This version does not add new features or corrections compared to version 1.0.0.alpha2.
It is only a version number upgrade to act that it is a public release version.
