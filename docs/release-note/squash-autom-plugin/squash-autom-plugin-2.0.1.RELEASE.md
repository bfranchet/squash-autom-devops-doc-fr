# Squash AUTOM plugin 2.0.1 Release Note

## Introduction

Squash AUTOM plugin is a plugin for Squash TM to allow automated items execution from Squash TM by Squash AUTOM.

This release note describes the changes of Squash AUTOM plugin version 2.0.1 compared to version 2.0.0.

Squash AUTOM plugin 2.0.1 is compatible with Squash TM version 2.0.0 or higher.

Squash AUTOM plugin 2.0.1 was released on September 9, 2021.

The plugin exists in two versions: Community (free) and Premium (commercial).

----

The new version only contains some technical internal improvements.
