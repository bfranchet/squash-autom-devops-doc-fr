# Squash AUTOM plugin 1.0.3 Release Note

## Introduction

Squash AUTOM plugin is a plugin for Squash TM to allow automated items execution from Squash TM by Squash AUTOM.

This release note describes the changes of Squash AUTOM plugin version 1.0.3 compared to version 1.0.2.

Squash AUTOM plugin 1.0.3 is compatible with Squash TM version 1.22.2 or higher, of type 1.22.X.

Squash AUTOM plugin 1.0.3 was released on October 21, 2021.

The plugin exists in two versions: Community (free) and Premium (commercial).

----

The new version only contains some technical internal improvements (including proper start/stop of the orchestrator).
