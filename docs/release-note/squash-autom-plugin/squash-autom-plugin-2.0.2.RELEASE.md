# Squash AUTOM plugin 2.0.2 Release Note

## Introduction

Squash AUTOM plugin is a plugin for Squash TM to allow automated items execution from Squash TM by Squash AUTOM.

This release note describes the changes of Squash AUTOM plugin version 2.0.2 compared to version 2.0.1.

Squash AUTOM plugin 2.0.2 is compatible with Squash TM version 2.0.0 or higher.

Squash AUTOM plugin 2.0.2 was released on October 21, 2021.

The plugin exists in two versions: Community (free) and Premium (commercial).

----

The new version only contains some technical internal improvements (including proper start/stop of the orchestrator).
