# Squash Autom plugin 1.0.0.alpha2 Release Note

## Introduction

Squash AUTOM plugin is a plugin for Squash TM to allow automated items execution from Squash TM by Squash AUTOM.

This release note explains new features of Squash AUTOM plugin version 1.0.0.alpha2.

Squash AUTOM plugin 1.0.0.alpha2 is compatible with Squash TM version 1.22.2.RELEASE.

Squash AUTOM plugin 1.0.0.alpha2 was released on March 16, 2021.

The plugin exists in two versions: Community (free) and Premium (commercial).

----

## Major features of Community version

* Squash TM administrator can declare a Squash AUTOM automation server in Squash TM.
* Squash TM user can launch automated item execution by Squash AUTOM.
* Following information about automated item is sent to Squash Orchestrator:
    * Test case dataset name
    * Test case dataset's parameters values
    * Test case custom fields values

## Major features of Premium version

* Squash TM administrator can declare a Squash AUTOM automation server in Squash TM.
* Squash TM user can launch automated item execution by Squash AUTOM.
* Following information about automated item is sent to Squash Orchestrator:
    * Test case dataset name
    * Test case dataset's parameters values
    * Test case custom fields values
    * Iteration custom fields values
    * Test Suite custom fields values
    * Campaign custom fields values
