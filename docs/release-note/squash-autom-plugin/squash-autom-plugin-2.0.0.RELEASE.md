# Squash AUTOM plugin 2.0.0 Release Note

## Introduction

Squash AUTOM plugin is a plugin for Squash TM to allow automated items execution from Squash TM by Squash AUTOM.

This release note describes the changes of Squash AUTOM plugin version 2.0.0 compared to version 1.0.0.

Squash AUTOM plugin 2.0.0 is compatible with Squash TM version 2.0.0 or higher.

Squash AUTOM plugin 2.0.0 was released on July 15, 2021.

The plugin exists in two versions: Community (free) and Premium (commercial).

----

This version is compatible with Squash TM 2.X.Y, starting 2.0.0.

## Plugin Improvement

* [SQUASH-3659] Refactoring of the plugin to use a library shared with other modules of Squashtest project
