# Squash AUTOM plugin 1.0.1 Release Note

## Introduction

Squash AUTOM plugin is a plugin for Squash TM to allow automated items execution from Squash TM by Squash AUTOM.

This release note describes the changes of Squash AUTOM plugin version 1.0.1 compared to version 1.0.0.

Squash AUTOM plugin 1.0.1 is compatible with Squash TM version 1.22.2 or higher, of type 1.22.X.

Squash AUTOM plugin 1.0.1 was released on July 15, 2021.

The plugin exists in two versions: Community (free) and Premium (commercial).

----

## Plugin Improvement

* [SQUASH-3659] Refactoring of the plugin to use a library shared with other modules of Squashtest project
