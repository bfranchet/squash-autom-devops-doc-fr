# Orchestrator Tools 0.20.1 Release Note

## Introduction

The Orchestrator Tools simplify the management of the orchestrator.

Orchestrator Tools 0.20.1 were released on October 21, 2021. 

---

## New features

* Addition of `opentf-ready` (see <a href="https://opentestfactory.gitlab.io/orchestrator/tools/start-and-wait/" target="_blank">the script documentation</a>) to wait until the orchestrator is ready to accept workflows.

* Addition of `opentf-done` (see <a href="https://opentestfactory.gitlab.io/orchestrator/tools/wait-and-stop/" target="_blank">the script documentation</a>) to wait until the orchestrator can be safely stopped (i.e. it has no more pending tasks).

* Addition of `opentf-ctl` (see <a href="https://opentestfactory.gitlab.io/orchestrator/tools/running-commands/" target="_blank">the script documentation</a>) that can be used to

    * start a workflow
    * stop a workflow
    * cancel a workflow
    * generate a signed token
    * list the subscriptions on the event bus
