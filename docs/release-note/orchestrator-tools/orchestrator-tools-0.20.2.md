# Orchestrator Tools 0.20.2 Release Note

## Introduction

The Orchestrator Tools simplify the management of the orchestrator.

This release note describes the changes of Orchestrator Tools version 0.20.2 compared to 0.20.1.

Orchestrator Tools 0.20.2 were released on November 16, 2021. 

---

The `opentf-ready` tool has been updated to properly handle the microservice in charge of generating Allure reports.
