# Test Plan Retriever plugin 2.0.0 Release Note

## Introduction

Test Plan Retriever plugin is a plugin for Squash TM to send informations about an automated execution plan to a Squash Orchestrator instance.

This release note describes the changes of Test Plan Retriever plugin version 2.0.0 compared to 1.0.0.

Test Plan Retriever plugin 2.0.0 is compatible with Squash TM version 2.0.0 or higher.

Test Plan Retriever plugin 2.0.0 was released on July 15, 2021.

The plugin exists in two versions: Community (free) and Premium (commercial).

----

This version is equivalent to 1.0.0 version but compatible with Squash TM 2.X.Y, starting 2.0.0.
