# Test Plan Retriever plugin 1.0.0.alpha1 Release Note

## Introduction

Test Plan Retriever plugin is a plugin for Squash TM to send informations about an automated execution plan to a Squash Orchestrator instance.

This release note explains new features of Test Plan Retriever plugin version 1.0.0.alpha1.

Test Plan Retriever plugin 1.0.0.alpha1 was released on February 05, 2021.

The plugin exists in two versions: Community (free) and Premium (commercial).

## Major features of Community version

* Can send informations about automated item of an Iteration execution plan
* Can send informations about automated item of a Test Suite execution plan
* Returns following details about an item:
    * Test case dataset name
    * Test case dataset parameters
    * Test case custom fields values

## Major features of Premium version

* Can send informations about automated item of an Iteration execution plan
* Can send informations about automated item of a Test Suite execution plan
* Can filter an execution plan by the value of a test case's tag custom field
* Returns following details about an item:
    * Test case dataset name
    * Test case dataset parameters
    * Test case custom fields values
    * Iteration custom fields values
    * Test Suite custom fields values
    * Campaign custom fields values
