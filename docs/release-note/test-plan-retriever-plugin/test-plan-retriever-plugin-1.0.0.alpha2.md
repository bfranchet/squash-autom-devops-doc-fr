# Test Plan Retriever plugin 1.0.0.alpha2 Release Note

## Introduction

Test Plan Retriever plugin is a plugin for Squash TM to send informations about an automated execution plan to a Squash Orchestrator instance.

This release note describes the changes of Test Plan Retriever plugin version 1.0.0.alpha2 compared to 1.0.0.alpha1.

Test Plan Retriever plugin 1.0.0.alpha2 is compatible with Squash TM version 1.22.2.RELEASE.

Test Plan Retriever plugin 1.0.0.alpha2 was released on March 16, 2021.

The plugin exists in two versions: Community (free) and Premium (commercial).

----

## Major new features of Community version

* [SQUASH-2636] Send branch and authentification information about scm repository where an automated test case is located.

## Major new features of Premium version

* [SQUASH-2636] Send branch and authentification information about scm repository where an automated test case is located.
