# Squash Orchestrator Premium Services 3.0.1 Release Note

## Introduction

Squash Orchestrator Premium Services is a set of micro-services exclusive to Squash AUTOM Premium users to perform automated test execution workflows described in a specific format.

This release note describes the changes of Squash Orchestrator Premium Services version 3.0.1 compared to 3.0.0.

Squash Orchestrator Premium Services 3.0.1 was released on September 9, 2021.

----

The new version only contains some technical internal improvements.
