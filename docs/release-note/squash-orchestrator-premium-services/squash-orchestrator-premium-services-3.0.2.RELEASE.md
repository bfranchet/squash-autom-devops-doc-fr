# Squash Orchestrator Premium Services 3.0.2 Release Note

## Introduction

Squash Orchestrator Premium Services is a set of micro-services exclusive to Squash AUTOM Premium users to perform automated test execution workflows described in a specific format.

This release note describes the changes of Squash Orchestrator Premium Services version 3.0.2 compared to 3.0.1.

Squash Orchestrator Premium Services 3.0.2 was released on October 21, 2021.

----

The new version only contains some technical internal improvements (including proper start/stop of the orchestrator).
