# Squash Orchestrator Premium Services 2.0.0.RELEASE Release Note

## Introduction

Squash Orchestrator Premium Services is a set of micro-services exclusive to Squash AUTOM Premium users to perform automated test execution workflows described in a specific format.

This release note describes the changes of Squash Orchestrator Premium Services version 2.0.0.RELEASE compared to 1.0.1.RELEASE.

Squash Orchestrator Premium Services 2.0.0.RELEASE was released on June 04, 2021.

----

## Major features of the release

* Exploit following actions in yours PEaC:
    * Launch Ranorex tests

----

## New micro-services introduced by the release

### **Ranorex report interpretor service**

Service to extract execution results from a Ranorex type xml report.

### **Ranorex action provider service**

Service to allow Ranorex test related actions inside a PEaC.

Following actions are supported:

* Execute a Ranorex test
* Generate parameters to be used during Ranorex test execution
