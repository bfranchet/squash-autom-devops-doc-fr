# Squash Orchestrator Premium Services 1.0.0.RELEASE Release Note

## Introduction

Squash Orchestrator Premium Services is a set of micro-services exclusive to Squash AUTOM Premium users to perform automated test execution workflows described in a specific format.

This release note explains new features of Squash Orchestrator Premium Services version 1.0.0.RELEASE.

Squash Orchestrator Premium Services 1.0.0.RELEASE was released on April 23, 2021.

----

## Major features of the release

* New Docker Image for Squash AUTOM Premium only micro-services. Following micro-services are added to this image:
    * Agilitest action provider service
    * Agilitest report interpretor service
* Exploit following actions in yours PEaC:
    * Launch Agilitest tests
    * Exploitation of Agilitest results in generated Allure report

----

## New micro-services introduced by the release

### **Agilitest report interpretor service**

Service to extract execution results from a Agilitest type xml report.

### **Agilitest action provider service**

Service to allow Cucumber test related actions inside a PEaC.

Following actions are supported:

* Execute an Agilitest test
* Generate parameters to be used during test execution
