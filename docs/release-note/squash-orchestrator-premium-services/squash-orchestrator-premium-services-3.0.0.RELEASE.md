# Squash Orchestrator Premium Services 3.0.0 Release Note

## Introduction

Squash Orchestrator Premium Services is a set of micro-services exclusive to Squash AUTOM Premium users to perform automated test execution workflows described in a specific format.

This release note describes the new features of Squash Orchestrator Premium Services version 3.0.0 compared to 2.0.0.

Squash Orchestrator Premium Services 3.0.0 was released on July 15, 2021.

----

## Major features of the release

* Exploit following actions in yours PEaC:
    * Launch UFT tests (UFT GUI Test only, not UFT Service Test)
    * Exploitation of UFT results in generated Allure report
    * Exploitation of Ranorex results in generated Allure report

----

## New micro-services introduced by the release

### **UFT report interpretor service**

Service to extract execution results from a UFT type xml report.

### **UFT action provider service**

Service to allow UFT test related actions inside a PEaC.

Following actions are supported:

* Execute a UFT test (UFT GUI Test only, not UFT Service Test)
* Generate parameters to be used during UFT test execution

----

## Bug fixes

* Bugs related to distributed deployment of Squash Orchestrator Docker image and Squash Orchestrator Premium Services Docker image
    * [SQUASH-3892] Ranorex action provider service does not use BUS_HOST and BUS_PORT environment variables when starting
    * [SQUASH-3909] Services starts on the local IP adress
