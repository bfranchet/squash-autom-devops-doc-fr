# Java Param Library 1.0.0 Release Note

## Introduction

The Java Param library is a Java API that can be used in automated tests to retrieve the value of some test parameters defined in Squash TM.

This release is the first version of the library.

Java Param Library 1.0.0 was released on September 9, 2021.
