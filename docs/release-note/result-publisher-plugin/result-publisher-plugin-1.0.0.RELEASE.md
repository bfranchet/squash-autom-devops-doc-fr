# Result publisher plugin 1.0.0.RELEASE Release Note

## Introduction

Result publisher plugin is a plugin for Squash TM to handle test results of automated ITPI received from a Squash Orchestrator instance.

This release note describes the changes of Result publisher plugin version 1.0.0.RELEASE compared to 1.0.0.alpha2.

Result publisher plugin 1.0.0.RELEASE is compatible with Squash TM version 1.22.2.RELEASE or higher.

Result publisher plugin 1.0.0.RELEASE was released on April 23, 2021.

The plugin exists in two versions: Community (free) and Premium (commercial).

----

## Bug fixes

* [SQUASH-3400] Set test execution statuts result URL to null to avoid unnecessary warning log in Squash TM when a result is received.
