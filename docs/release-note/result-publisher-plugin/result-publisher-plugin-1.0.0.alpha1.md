# Result publisher plugin 1.0.0.alpha1 Release Note

## Introduction

Result publisher plugin is a plugin for Squash TM to handle test results of automated ITPI received from a Squash Orchestrator instance.

This release note explains new features of Result publisher plugin version 1.0.0.alpha1.

Result publisher plugin 1.0.0.alpha1 was released on February 05, 2021.

The plugin exists in two versions: Community (free) and Premium (commercial).

## Major features of Community version

* Update Automated Suite status according to an automated test execution result
* Update ITPI status according to the linked automated test execution result
* Execution reports of an automated test execution is attach to the corresponding automated suite

## Major features of Premium version

* Update Automated Suite status according to an automated test execution result
* User can choose between two types of result publication for a project:
    * Light: Update ITPI status according to the linked automated test execution result. Execution reports of an automated test execution is attach to the corresponding automated suite.
    * Complete: Create a new automated execution for an ITPI with status based on received result. Execution reports of an automated test execution is attach to the newly created ITPI's execution.
