# Result publisher plugin 2.1.0 Release Note

## Introduction

Result publisher plugin is a plugin for Squash TM to handle test results of automated ITPI received from a Squash Orchestrator instance.

This release note describes the changes of Result publisher plugin version 2.1.0 compared to 2.0.0.

Result publisher plugin 2.1.0 is compatible with Squash TM version 2.1.0 or higher.<br>
Result publisher plugin 2.0.0 is compatible with Squash TM version 2.0.X.<br>
Result publisher plugin 1.0.0 is compatible with Squash TM version 1.22.X, starting with 1.22.2 version.

Result publisher plugin 2.1.0 was released on September 9, 2021.

The plugin exists in two versions: Community (free) and Premium (commercial).
