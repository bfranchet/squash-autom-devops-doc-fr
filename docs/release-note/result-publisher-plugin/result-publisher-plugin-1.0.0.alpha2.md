# Result publisher plugin 1.0.0.alpha2 Release Note

## Introduction

Result publisher plugin is a plugin for Squash TM to handle test results of automated ITPI received from a Squash Orchestrator instance.

This release note describes the changes of Result publisher plugin version 1.0.0.alpha2 compared to 1.0.0.alpha1.

Result publisher plugin 1.0.0.alpha2 is compatible with Squash TM version 1.22.2.RELEASE.

Result publisher plugin 1.0.0.alpha2 was released on March 16, 2021.

The plugin exists in two versions: Community (free) and Premium (commercial).

----

## Major features of Community version

* [SQUASH-2348] Synthetic allure report can be linked to an Automated Suite

## Major features of Premium version

* [SQUASH-2348] Synthetic allure report can be linked to an Automated Suite

----

## Bug fixes

* [SQUASH-2749] Change attachment names for Community version to be same than in Premium version
