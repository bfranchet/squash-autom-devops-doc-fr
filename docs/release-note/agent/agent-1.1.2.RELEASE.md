# Agent 1.1.2 Release Note

## Introduction

The agent is a process running on an execution environment. It contacts the orchestrator at regular intervals, looking for some orders to execute. If there is any order, the agent will perform it and send the result back to the orchestrator.

This release note describes the changes of Agent version 1.1.2 compared to 1.0.1.

Agent 1.1.2 was released on October 21, 2021. 

---

The new version contains the fix of a latent problem which became active with the new *put-file* action of the Orchestrator.
