# Appel au Squash Orchestrator depuis un pipeline Jenkins

## Configuration d’un Squash orchestrator dans Jenkins

Pour accéder à l’espace de configuration du **Squash Orchestrator**, il faut tout d’abord se rendre dans l’espace *Configurer le système* du *System Configuration*, accessible par l’onglet *Administrer Jenkins* :

![jenkins-system-configuration](resources/jenkins-system-configuration.png){class="center"}

Un bloc nommé *Squash Orchestrator servers* sera ensuite disponible :

![jenkins-orchestrator-server](resources/jenkins-squash-orchestrator-server.png){class="fullpage"}

* ``Server id`` : Cet ID est généré automatiquement et ne peut être modifié. Il n’est pas utilisé par l’utilisateur.

* ``Server name`` : Ce nom est défini par l’utilisateur. C’est celui qui sera mentionné dans le script du pipeline lors d’une exécution de workflow.

* ``Receptionist endpoint URL`` : L’adresse du micro-service *receptionist* de l'orchestrateur, avec son port tel que
  défini au lancement de l'orchestrateur.  
  Reportez-vous à la [documentation de **Squash Orchestrator**](../autom/install.md#configuration-de-limage) pour plus de détails.

* ``Workflow Status endpoint URL`` : L’adresse du micro-service *observer* de l'orchestrateur, avec son port tel que
  défini au lancement de l'orchestrateur.
  Reportez-vous à la documentation de [documentation de **Squash Orchestrator**](../autom/install.md#configuration-de-limage) pour plus de détails.

* ``Credential`` : Identifiant *Jenkins* de type *Secret text* contenant un *JWT Token* permettant de s’authentifier auprès de l'orchestrateur.  
Reportez-vous à la <a href="https://opentestfactory.gitlab.io/orchestrator/installation/#trusted-keys" target="_blank">documentation de l'**OpenTestFactory Orchestrator**</a> pour plus de détails sur l’accès sécurisé à l'orchestrateur.

* ``Workflow Status poll interval`` : Ce paramètre correspond au temps de mise à jour du statut du workflow.

* ``Workflow creation timeout`` : Timeout sur la réception du PEaC par le *receptionist* côté orchestrateur.

## Intégration de l'appel à Squash Orchestrator dans un pipeline Jenkins

Une fois qu'il y a au moins un **Squash Orchestrator** configuré dans *Jenkins*, il est possible de faire appel à l’orchestrateur depuis un job *Jenkins* de type pipeline grâce à une méthode de pipeline dédiée.

Ci-dessous, un exemple de pipeline simple utilisant la méthode d’appel à l’orchestrateur :

``` bash
node {
   stage 'Stage 1: sanity check'
   echo 'OK pipelines work in the test instance'
   stage 'Stage 2: steps check'
   configFileProvider([configFile(
fileId: '600492a8-8312-44dc-ac18-b5d6d30857b4',
targetLocation: 'testWorkflow.json'
)]) {
    def workflow_id = runSquashWorkflow(
        workflowPathName:'testWorkflow.json',
        workflowTimeout: '300S',
        serverName:'defaultServer'
    )
    echo "We just ran The Squash Orchestrator workflow $workflow_id"
   }
}
```

La méthode *runSquashWorkflow* permet de transmettre un PEaC à l'orchestrateur pour exécution.

Elle dispose de 3 paramètres :

* ``workflowPathName`` : Le chemin vers le fichier contenant le PEaC. Dans le cas présent, le fichier est injecté via le plugin *Config File Provider*, mais il est également possible de l’obtenir par d’autres moyens (récupération depuis un SCM, génération à la volée dans un fichier, ...).

* ``workflowTimeout`` : Timeout sur l’exécution des actions. Ce paramètre intervient par exemple si un environnement n’est pas joignable (ou n’existe pas), ou si une action n’est pas trouvée par un *actionProvider*. Il est à adapter en fonction de la durée d’exécution attendue des différents tests du PEaC.

* ``serverName`` : Nom du serveur **Squash Orchestrator** à utiliser. Ce nom est celui défini dans l’espace de configuration *Squash Orchestrator servers* de *Jenkins*.

### Appel pour le déclenchement d'un PEaC exploitant l'environnement d'exécution `inception`

Lorsque le but est de faire appel à Squash Orchestrator pour l'exécution d'un PEaC utilisant l'environnement d'exécution `inception`, tel que décrit dans [la documentation](./callWithPeac.md#comment-utiliser-lenvironnement-inception-au-sein-dun-script), le pipeline est à adapter suivant le modèle ci-dessous :

``` bash
node {
   stage 'Stage 1: sanity check'
   echo 'OK pipelines work in the test instance'
   stage 'Stage 2: steps check'
   configFileProvider([
            configFile(fileId: 'inception_peac.yaml', targetLocation: 'testWorkflow.yaml'),
            configFile(fileId: 'TEST-ForecastSuite.xml', targetLocation: 'TEST-ForecastSuite.xml')
    ]) {
       def workflow_id = runSquashWorkflow(
           workflowPathName:'testWorkflow.yaml',
           workflowTimeout: '300S',
           serverName:'defaultServer',
           fileResources: [ref('name':'myfile','path':'TEST-ForecastSuite.xml')]
       )

       echo "We just ran The Squash Orchestrator workflow $workflow_id"
   }
}
```

Un nouveau paramètre apparait pour la méthode *runSquashWorkflow* :

* ``fileResources`` : ce paramètre permet de renseigner les fichiers complémentaires au PEaC à transmettre à l'orchestrateur. Ces fichiers correspondent à ceux renseignés dans la section `files` du PEaC. Il se compose d'un tableau d'éléments `ref` qui sont composés de deux attributs :
    * `name` : correspond au nom du fichier tel que renseigné dans le PEaC
    * `path` : le chemin du fichier au sein du workspace du job Jenkins.
