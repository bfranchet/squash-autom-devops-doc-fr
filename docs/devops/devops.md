# Squash DEVOPS

Ce guide présente les possibilités offertes par **Squash DEVOPS** pour exécuter des tests automatiques à partir d'un pipeline de CI/CD. La gestion de ces tests et du workflow de leur automatisation dans **Squash TM** est décrite dans [la documentation](https://tm-fr.doc.squashtest.com/user-guide/gestion-tests-automatises/concevoir-tests-automatises/) de ce dernier.

**Squash DEVOPS** met à votre disposition les composants suivants (voir schéma ci-dessous) :

--8<--
shared/commonComponents.md
--8<--

* **Plugins pour Squash TM**

    * **Result Publisher**† : ce plugin permet la remontée d’informations vers **Squash TM** en fin d’exécution d’un plan d’exécution **Squash TM** par l’orchestrateur Squash.  
    Ce plugin existe en version **Community** (*squash.tm.rest.result.publisher.community*) librement téléchargeable ou **Premium** (*squash.tm.rest.result.publisher.premium*) accessible sur demande.

    * **Test Plan Retriever** : ce plugin permet l’envoi à **Squash Orchestrator** de détails sur un plan d’exécution **Squash TM**.  
    Ce plugin existe en version **Community** (*squash.tm.rest.test.plan.retriever.community*) librement téléchargeable ou **Premium** (*squash.tm.rest.test.plan.retriever.premium*) accessible sur demande.

* **Plugin Squash DEVOPS pour Jenkins**  
Ce plugin pour *Jenkins* facilite l’envoi d’un PEaC à Squash Orchestrator depuis un pipeline *Jenkins*.

Schéma de l'architecture haut niveau Squash AUTOM / Squash DEVOPS :
![schéma d'architecture](../resources/architecture.png){class=fullpage}
---
† indique un composant commun à **Squash AUTOM** et **Squash DEVOPS**.
