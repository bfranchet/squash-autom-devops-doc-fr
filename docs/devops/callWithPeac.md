# Récupération d’un plan d’exécution Squash TM depuis un PEaC

**Squash DEVOPS** vous donne la possibilité de récupérer un plan d’exécution de tests automatisés défini dans **Squash TM** depuis un PEaC, ce PEaC pouvant être déclenché depuis un pipeline *Jenkins* par exemple (voir la [page correspondante](./callFromJenkins.md) de ce guide).

!!! warning "Focus"
    Pour le bon fonctionnement de cette fonctionnalité, les plugins Test Plan Retriever et Result Publisher doivent être installés sur l'instance Squash TM ciblée.

## Prérequis

Un utilisateur appartenant au groupe *Serveur d’automatisation de tests* doit exister dans **Squash TM**.

## Intégration de l’étape de récupération d’un plan d’exécution Squash TM dans un PEaC

Dans **Squash TM**, le plan d’exécution, itération ou suite de tests, doit contenir au moins un ITPI (Iteration Test Plan Item, voir le [glossaire Squash TM](https://tm-fr.doc.squashtest.com/introduction/#terminologie)) lié à un cas de test automatisé suivant [les instructions du guide utilisateur](../autom/pilotFromSquash.md#automatisation-dun-cas-de-test-squash-tm) **Squash AUTOM**.

Pour récupérer ce plan d’exécution dans un PEaC, il faut faire appel à l’action *generator* correspondante.

Voici un exemple simple d'un tel PEaC au format *JSON* :

``` json
{
    "apiVersion": "opentestfactory.org/v1alpha1",
    "kind": "Workflow",
    "metadata": {
        "name": "Simple Workflow"
    },
    "defaults": {
        "runs-on":"ssh"
    },
    "jobs": {
        "explicitJob": {
            "runs-on":"ssh",
            "generator":"tm.squashtest.org/tm.generator@v1",
            "with": {
                "testPlanUuid":"1e2ae123-6b67-44b2-b229-274ea17ad489",
                "testPlanType":"Iteration",
                "squashTMUrl":"https://squashtm.example.com/squash",
                "squashTMAutomatedServerLogin":"tfserver",
                "squashTMAutomatedServerPassword":"tfserver",
                "technologyLabels":{
                    "ranorex": ["windows","ranorex"],
                    "robotframework": ["linux","robotframework"],
                    "junit": ["linux","junit"]
                }
            }
        }
    }
}
```

Un step *generator* **Squash TM** doit contenir les paramètres suivants :

* ``testPlanType`` : Correspond au type du plan de test à récupérer dans **Squash TM**. Seules les valeurs *Iteration* et *TestSuite* sont acceptées.

* ``testPlanUuid`` : Correspond à l’UUID (Universally Unique IDentifier, voir la [documentation Squash TM](https://tm-fr.doc.squashtest.com/user-guide/gestion-executions/creer-organiser-objets-campagnes/#les-attributs-des-objets-de-lespace-campagnes)) du plan de test souhaité. Celui-ci peut être récupéré dans le bloc Information de l’itération ou de la suite de tests souhaitée dans **Squash TM**.

* ``squashTMUrl`` : URL du **Squash TM** à viser.

* ``squashTMAutomatedServerLogin`` : Nom de l’utilisateur du groupe *Serveur d’automatisation de tests* à utiliser dans **Squash TM**.

* ``squashTMAutomatedServerPassword`` : Mot de passe de l’utilisateur du groupe *Serveur d’automatisation de tests* à utiliser dans **Squash TM**.

[*Champs Optionnels*] :

* ``tagLabel`` : Spécifique à la version **Premium** - Correspond au nom du champ personnalisé de type *tag* sur lequel on souhaite filtrer les cas de test à récupérer. Il n’est pas possible d’en spécifier plusieurs.

* ``tagValue`` : Spécifique à la version **Premium** - Correspond à la valeur du champ personnalisé de type *tag* sur lequel on souhaite filtrer les cas de test à récupérer. Il est possible d’indiquer plusieurs valeurs séparées par des “|” (*Exemple :* valeur1|valeur2). Il faut au moins l’une des valeurs pour que le cas de test soit pris en compte.

!!! warning "Focus"
    Si l’un des deux champs *tagLabel* ou *tagValue* est présent, alors l’autre champ **doit** également être renseigné.

* ``technologyLabels`` : A utiliser notamment si vous récupérez un plan d'exécution contenant des tests devant s'exécuter sur des environnements différents.  
  Il permet de spécifier pour chaque framework d'automatisation les étiquettes de l'environnement d'exécution à cibler.  
  Celles-ci seront ajoutées aux étiquettes du runs-on du job Generator.  
  Pour une technologie donnée, les étiquettes se précisent en ajoutant une entrée à la liste du champ sous la forme ``"préfixe de la technologie": ["etiquette1", "etiquette2"]``

!!! info "Info"
    Par défaut, les jobs créés par le Generator suite à la récupération d'un plan d'exécution se voient attribués une étiquette correspondant au préfixe de la technologie de test (nom de la technologie en minuscule sans espace, par exemple "robotframework" pour Robot Framework) en plus de celles renseignées dans le runs-on du job generator.

## Utilisation avec l'environnement d'exécution `inception` de l'orchestrateur

OpenTestFactory, et de fait Squash DEVOPS, propose un environnement d'exécution spécial nommé `inception` qui permet un mode d'utilisation alternatif des jobs de type `generator`.

### À quoi sert l'environnement `inception` ?

Le processus normal d'exécution d'un job `generator` est d'aller récupérer un plan d'exécution depuis une instance de **Squash TM** puis d'aller exécuter les tests automatisés de ce plan sur les environnements d'exécution dédiés.
Lorsque l'environnement d'exécution `inception` est utilisé pour le job `generator`, il n'y a pas de tests exécutés suite à la récupération du plan d'exécution. À la place, il y a directement l'étape de remontée d'information vers **Squash TM** basée sur un ensemble de rapports transmis à l'orchestrateur en amont.

### Comment utiliser l'environnement `inception` au sein d'un PEaC

Voici un exemple de workflow au format `YAML` utilisant l'environnement d'exécution `inception` et allant récupérer un plan d'exécution Squash TM.

``` yaml
metadata:
  name: Test Inception
resources:
  files:
  - report1
  - report2
  - report3
jobs:
  prepare:
    runs-on: inception
    steps:
    - uses: actions/prepare-inception@v1
      with:
        report.html: ${{ resources.files.report1 }}
        output.xml: ${{ resources.files.report2 }}
        log.html: ${{ resources.files.report3 }}
  robot:
    runs-on: inception
    needs: [prepare]
    generator: tm.squashtest.org/tm.generator@v1
    with:
      squashTMUrl: https://squashtm.example.com/squash
      squashTMAutomatedServerLogin: ${{ variables.SQUASH_USER }}
      squashTMAutomatedServerPassword: ${{ variables.SQUASH_PASSWORD }}
      testPlanUuid: 1e2ae123-6b67-44b2-b229-274ea17ad489
      testPlanType: Iteration
```

Plusieurs différences sont à noter par rapport au PEaC présenté à la [section précédente](#integration-de-letape-de-recuperation-dun-plan-dexecution-squash-tm-dans-un-peac) :

* La section `files` dans la partie `resources` : elle permet de lister les ressources fournies en entrée à l'orchestrateur. C'est à partir de ces données que seront extraites les informations remontées à Squash TM.

* Un job de préparation, intitulé `prepare` dans cet exemple : il utilise l'action provider `actions/prepare-inception@v1` et permet d'indiquer à quelle ressource normalement générée par le test correspond chaque ressource renseignée dans le bloc `files`

* Le job `generator` : il est très semblable à la version de la [section précédente](#integration-de-letape-de-recuperation-dun-plan-dexecution-squash-tm-dans-un-peac) à deux points près :
    * Il utilise l'environnement `inception` dans son champ `runs-on`
    * Il précise que le job de préparation est un pré-requis pour son exécution via la ligne `needs: [prepare]`

Un tel PEaC peut être exécuté via l'appel suivant :

``` bash
curl -X POST \
     -H "Authorization: Bearer ${TOKEN}" \
     -F workflow=@Squashfile \
     -F report1=@report1.html \
     -F report2=@report2.xml \
     -F report3=@report3.xml \
     -F variables="SQUASH_USER=${USER}\nSQUASH_PASSWORD=${PASSWD}" \
     http://orchestrator.example.com/workflows
```

Il est à noter dans cet appel le passage en paramètre des rapports renseignés dans le PEaC.

Un PEaC exploitant l'environnement d'exécution `inception` peut aussi être déclenché [depuis un pipeline Jenkins grâce au step proposé par le plugin *Squash DEVOPS*](./callFromJenkins.md#appel-pour-le-declenchement-dun-peac-exploitant-lenvironnement-dexecution-inception).

## Paramètres Squash TM exploitables dans un test automatisé

En exécutant un PEaC avec récupération d’un plan d’exécution **Squash TM**, ce dernier transmet différentes informations sur les éléments du plan d'exécution qu’il est possible d’exploiter dans un cas de test.

Pour plus d'informations, veuillez consulter la section [Exploitation de paramètres Squash TM](../autom/pilotFromSquash.md#exploitation-de-parametres-squash-tm) de la documentation de **Squash AUTOM**, ainsi que la section dédiée à l'automatisation du framework de test souhaité.

--8<--
shared/frameworkSupportingParam.md
--8<--

## Remontée d’informations vers Squash TM en fin d’exécution

La nature de la remontée d’informations à **Squash TM** en fin d’exécution d’un plan d’exécution **Squash TM** va dépendre de si vous êtes sous licence **Squash AUTOM Community** ou **Squash AUTOM Premium**.

Consultez le [guide utilisateur **Squash AUTOM**](../autom/pilotFromSquash.md#remontees-de-resultats-apres-execution-dun-plan-dexecution-squash-tm) pour plus d'informations.
