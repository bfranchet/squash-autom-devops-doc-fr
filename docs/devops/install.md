# Guide d'Installation

--8<--
shared/installation/squashOrchestratorInstallation.md

shared/installation/orchestratorToolsInstallation.md

shared/installation/otfAgentInstallation.md

shared/installation/technologySpecifities.md
--8<--

## Plugins Squash TM

### Installation

Pour l’installation des plugins **Squash TM**, merci de vous reporter au [protocole d’installation d’un plugin **Squash TM**](https://tm-fr.doc.squashtest.com/install-guide/installation-plugins/installer-plugins/#installation-des-plugins-squash-tm).

<table>
 <thead>
  <tr>
   <th>Plugin</th>
   <th>Version de Squash TM</th>
   <th>Version compatible du plugin</th>
   <th>Télécharger la dernière version Community</th>
  </tr>
 </thead>
 <tbody>
  <tr>
   <td rowspan="3">Result Publisher†</td>
   <td>1.22.X, à partir de 1.22.2</td>
   <td>1.X.X</td>
   <td>1.0.0 : <a href="https://repo.squashtest.org/distribution/squash.tm.rest.result.publisher.community-1.0.0.RELEASE.tar.gz">.tar.gz</a> ou <a href="https://repo.squashtest.org/distribution/squash.tm.rest.result.publisher.community-1.0.0.RELEASE.zip">.zip</a></td>
  </tr>
  <tr>
   <td>2.0.X</td>
   <td>2.0.X</td>
   <td>2.0.0 : <a href="https://repo.squashtest.org/distribution/squash.tm.rest.result.publisher.community-2.0.0.RELEASE.tar.gz">.tar.gz</a> ou <a href="https://repo.squashtest.org/distribution/squash.tm.rest.result.publisher.community-2.0.0.RELEASE.zip">.zip</a></td>
  </tr>
  <tr>
   <td>2.1.0, ou postérieure</td>
   <td>2.1.X</td>
   <td>2.1.0 : <a href="https://repo.squashtest.org/distribution/squash.tm.rest.result.publisher.community-2.1.0.RELEASE.tar.gz">.tar.gz</a> ou <a href="https://repo.squashtest.org/distribution/squash.tm.rest.result.publisher.community-2.1.0.RELEASE.zip">.zip</a></td>
  </tr>
  <tr>
   <td rowspan="2">Test Plan Retriever</td>
   <td>1.22.X, à partir de 1.22.2</td>
   <td>1.X.X</td>
   <td>1.0.0 : <a href="https://repo.squashtest.org/distribution/squash.tm.rest.test.plan.retriever.community-1.0.0.RELEASE.tar.gz">.tar.gz</a> ou <a href="https://repo.squashtest.org/distribution/squash.tm.rest.test.plan.retriever.community-1.0.0.RELEASE.zip">.zip</a></td>
  </tr>
  <tr>
   <td>2.0.0, ou postérieure</td>
   <td>2.X.X</td>
   <td>2.0.0 : <a href="https://repo.squashtest.org/distribution/squash.tm.rest.test.plan.retriever.community-2.0.0.RELEASE.tar.gz">.tar.gz</a> ou <a href="https://repo.squashtest.org/distribution/squash.tm.rest.test.plan.retriever.community-2.0.0.RELEASE.zip">.zip</a></td>
  </tr>
  </tbody>
</table>

## Plugin Squash DEVOPS pour Jenkins

### Installation

Le plugin est sous la forme d’un fichier ``.hpi`` (*squash-devops-1.3.0.hpi*) librement [téléchargeable](https://repo.squashtest.org/distribution/squash-devops-1.3.0.hpi).

Pour l’installation, soumettez le plugin depuis l’onglet *Avancé* de l’écran de gestion des plugins de *Jenkins* :

![jenkins-plugin-upload](resources/squash-devops-plugin-jenkins-upload.png){class="center"}

### Compatibilité du plugin

Ce plugin est compatible avec une version *2.164.1* ou supérieure de *Jenkins*.

---
† indique un composant commun à **Squash AUTOM** et **Squash DEVOPS**. Il ne doit être installé qu'une seule fois pour les deux.
