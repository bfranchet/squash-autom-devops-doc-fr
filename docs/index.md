# Bienvenue sur la documentation de Squash AUTOM et Squash DEVOPS

## A propos de Squash AUTOM et Squash DEVOPS

Squash est une suite d’outils pour concevoir, automatiser, exécuter et industrialiser vos tests.
Basée sur un socle open source, la solution est modulable et facilement intégrable.

Au sein de cette suite :

* **Squash AUTOM** est un ensemble de composants pour gérer l'exécution de vos tests automatisés depuis Squash TM, ainsi que la publication et l'historisation des résultats dans Squash TM.

* **Squash DEVOPS** est un ensemble de composants pour gérer la récupération, depuis un pipeline CI/CD, d'un plan d'exécution Squash TM, suivi de l'exécution des tests automatisés qui le compose et enfin la publication et l'historisation des résultats dans Squash TM.

![La suite Squash](resources/suite_squash.png){class=fullpage}

## Contenu de ce site

Ce site contient les ressources suivantes :

* [Squash AUTOM](autom/autom.md) : documentation sur Squash AUTOM (installation des composants, utilisation des fonctionnalités)
* [Squash DEVOPS](devops/devops.md) : documentation sur Squash DEVOPS (installation des composants, utilisation des fonctionnalités)
* [FAQ](faq/faq.md)
* [Release Notes](release-note/release-note-by-version.md) : release notes des composants Squash AUTOM et Squash DEVOPS

## Documentations complémentaires

* [Squash TM](https://tm-fr.doc.squashtest.com/) : documentation de Squash TM, l'application de la suite Squash permettant de gérer et piloter les tests en contexte agile et traditionnel.
* [Squash AUTOM/Squash DEVOPS (Legacy)](https://squash-autom-devops-doc.readthedocs.io/fr/1.1.0.release/) : ancienne documentation de Squash AUTOM et Squash DEVOPS valable jusqu'à la version 1.1.0.
* [Squash TF](https://squash-tf.readthedocs.io/en/latest/) : documentation sur Squash TF, le prédécesseur de Squash AUTOM pour l'exécution des tests automatisés depuis Squash TM.
